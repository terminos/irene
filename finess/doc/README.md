# FINESS

* Unité de production: DREES
* Page de présentation: http://finess.sante.gouv.fr/
* Distribution: https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/
* Dernière version: 14/05/2021
* Version mise au format: 12/03/2021
* Mise à jour: tous les 2 mois

# Documentation d'entrée

* "définitions" et "nomenclatures": http://finess.sante.gouv.fr/
* cf. Catégories établissement (PDF, pas à jour avec les données).

2 notions importantes:
* "Etablissement FINESS": nofiness**et**
* "Entité juridique FINESS": nofiness**ej**

Un "FINESS juridique" peut "chapoter" plusieurs "FINESS établissement" (ci-après, les 114 nofiness**et** AP-HP, jusqu'à 690 pour ADOMA, ex. SONACOTRA):

| nofinesset  | nofinessej  | rs                                     | libcategetab                                                 |
|-----------|-----------|----------------------------------------|--------------------------------------------------------------|
| 920000122 | 750712184 | AGEPS APHP DE NANTERRE                 | Centre Hospitalier Régional (C.H.R.)                         |
| 750000341 | 750712184 | AGEPS APHP DE PARIS                    | Centre Hospitalier Régional (C.H.R.)                         |
| 830100012 | 750712184 | APHP HOPITAL SAN SALVADOUR             | Centre Hospitalier Régional (C.H.R.)                         |
| 940010259 | 750712184 | CATTP BOISSY ST LEGER                  | Centre d'Accueil Thérapeutique à temps partiel (C.A.T.T.P.)  |
| 750019648 | 750712184 | CATTP LA MAISON DES ADOLESCENTS        | Centre d'Accueil Thérapeutique à temps partiel (C.A.T.T.P.)  |
| 920027794 | 750712184 | CATTP MOURIER SOUSSEL PREVOT           | Centre d'Accueil Thérapeutique à temps partiel (C.A.T.T.P.)  |
| 600100101 | 750712184 | CH AP-HP LIANCOURT                     | Centre Hospitalier Régional (C.H.R.)                         |
| 750802845 | 750712184 | CLINIQUE DENTAIRE GARANCIERE APHP      | Centre Hospitalier Régional (C.H.R.)                         |
| 750801524 | 750712184 | CLINIQUE DENTAIRE JEAN DELIBERO APHP   | Centre Hospitalier Régional (C.H.R.)                         |
| 750809576 | 750712184 | CLINIQUE DENTAIRE STE PERINE APHP      | Centre Hospitalier Régional (C.H.R.)                         |
| 920812930 | 750712184 | CMP ADULTES GUY DE MAUPASSANT          | Centre Médico-Psychologique (C.M.P.)                         |
| 940002231 | 750712184 | CMP DE BOISSY ST LEGER                 | Centre Médico-Psychologique (C.M.P.)                         |
| 940810872 | 750712184 | CMP DE MAISONS ALFORT                  | Centre Médico-Psychologique (C.M.P.)                         |
| 750802258 | 750712184 | CMP ENFANTS APHP BICHAT                | Centre Médico-Psychologique (C.M.P.)                         |
| 750801797 | 750712184 | CMP ENFANTS APHP ROBERT DEBRE          | Centre Médico-Psychologique (C.M.P.)                         |
| 750833345 | 750712184 | CMP ENFANTS EUGENIE EBOUE              | Centre Médico-Psychologique (C.M.P.)                         |
| 750833337 | 750712184 | CMP ENFANTS TIPHAINE NECKER            | Centre Médico-Psychologique (C.M.P.)                         |
| 750033938 | 750712184 | CMP NEY HOPITAL BICHAT                 | Centre Médico-Psychologique (C.M.P.)                         |
| 750805228 | 750712184 | CSAPA ESPACE MURGER                    | Centre soins accompagnement prévention addictologie (CSAPA)  |
| 750000358 | 750712184 | CSAPA MONTE CHRISTO                    | Centre soins accompagnement prévention addictologie (CSAPA)  |
| 940019144 | 750712184 | CSAPA BICETRE                          | Centre soins accompagnement prévention addictologie (CSAPA)  |
| 750830945 | 750712184 | CSAPA CASSINI                          | Centre soins accompagnement prévention addictologie (CSAPA)  |
| 930812334 | 750712184 | CSAPA GENERALISTE AVICENNE             | Centre soins accompagnement prévention addictologie (CSAPA)  |
| 750018939 | 750712184 | ECOLE DE MASSEURS KINESITERAPEUTES     | Ecoles Formant aux Professions Sanitaires                    |
| 940802291 | 750712184 | ECOLE HOPITAL CHARLES FOIX (AP-HP)     | Ecoles Formant aux Professions Sanitaires                    |
| 940802317 | 750712184 | ECOLE HOPITAL EMILE ROUX (AP-HP)       | Ecoles Formant aux Professions Sanitaires                    |
| 830025276 | 750712184 | EEAP SAN SALVADOUR                     | Etablissement pour Enfants ou Adolescents Polyhandicapés     |
| 600013908 | 750712184 | EHPAD LIANCOURT AP-HP                  | Etablissement d'hébergement pour personnes âgées dépendantes |
| 750019119 | 750712184 | **ETABLISSEMENT DE FORMATION POLYVALENTE** | Ecoles Formant aux Professions Sanitaires et Sociales        |
| 940100076 | 750712184 | GHU EST SITE JEAN ROSTAND              | Centre Hospitalier Régional (C.H.R.)                         |
| 750100141 | 750712184 | GPE HOSP BROUSSAIS HEGP                | Centre Hospitalier Régional (C.H.R.)                         |
| 750100182 | 750712184 | GPE HOSP COCHIN SAINT VINCENT DE PAUL  | Centre Hospitalier Régional (C.H.R.)                         |
| 940170087 | 750712184 | HDJ CRETEIL                            | Centre Hospitalier Régional (C.H.R.)                         |
| 920029758 | 750712184 | HDJ GUY DE MAUPASSANT                  | Centre Hospitalier Régional (C.H.R.)                         |
| 620100016 | 750712184 | HOP MARITIME DE BERCK                  | Centre Hospitalier Régional (C.H.R.)                         |
| 750100026 | 750712184 | HOPITAL COCHIN TARNIER APHP            | Centre Hospitalier Régional (C.H.R.)                         |
| 640790150 | 750712184 | HOPITAL MARIN VILLE PARIS              | Centre Hospitalier Régional (C.H.R.)                         |
| 750806226 | 750712184 | HOSPITALISATION A DOMICILE APHP PARIS  | Centre Hospitalier Régional (C.H.R.)                         |
| 950100024 | 750712184 | HU EST PARISIEN SITE ROCHE GUYON APHP  | Centre Hospitalier Régional (C.H.R.)                         |
| 750100083 | 750712184 | HU EST PARISIEN SITE ROTHSCHILD APHP   | Centre Hospitalier Régional (C.H.R.)                         |
| 750100091 | 750712184 | HU EST PARISIEN SITE ST ANTOINE APHP   | Centre Hospitalier Régional (C.H.R.)                         |
| 750100273 | 750712184 | HU EST PARISIEN SITE TENON APHP        | Centre Hospitalier Régional (C.H.R.)                         |
| 750100109 | 750712184 | HU EST PARISIEN SITE TROUSSEAU APHP    | Centre Hospitalier Régional (C.H.R.)                         |
| 940100019 | 750712184 | HU HENRI MONDOR CHENEVIER APHP         | Centre Hospitalier Régional (C.H.R.)                         |
| 910100015 | 750712184 | HU HENRI MONDOR SITE CLEMENCEAU APHP   | Centre Hospitalier Régional (C.H.R.)                         |
| 910100031 | 750712184 | HU HENRI MONDOR SITE DUPUYTREN APHP    | Centre Hospitalier Régional (C.H.R.)                         |
| 940100050 | 750712184 | HU HENRI MONDOR SITE E ROUX APHP       | Centre Hospitalier Régional (C.H.R.)                         |
| 940100027 | 750712184 | HU HENRI MONDOR SITE HENRI MONDOR APHP | Centre Hospitalier Régional (C.H.R.)                         |
| 910100023 | 750712184 | HU HENRI MONDOR SITE JOFFRE APHP       | Centre Hospitalier Régional (C.H.R.)                         |
| 750100208 | 750712184 | HU NECKER ENFANTS MALADES APHP         | Centre Hospitalier Régional (C.H.R.)                         |
| 920100013 | 750712184 | HU OUEST SITE AMBROISE PARE APHP       | Centre Hospitalier Régional (C.H.R.)                         |
| 750801441 | 750712184 | HU PARIS CENTRE SITE BROCA APHP        | Centre Hospitalier Régional (C.H.R.)                         |
| 750100166 | 750712184 | HU PARIS CENTRE SITE COCHIN APHP       | Centre Hospitalier Régional (C.H.R.)                         |
| 750100018 | 750712184 | HU PARIS CENTRE SITE HOTEL DIEU APHP   | Centre Hospitalier Régional (C.H.R.)                         |
| 750100299 | 750712184 | HU PARIS IDF SITE SAINTE PERINE APHP   | Centre Hospitalier Régional (C.H.R.)                         |
| 920100039 | 750712184 | HU PARIS NORD SITE BEAUJON APHP        | Centre Hospitalier Régional (C.H.R.)                         |
| 750100232 | 750712184 | HU PARIS NORD SITE BICHAT APHP         | Centre Hospitalier Régional (C.H.R.)                         |
| 750041543 | 750712184 | HU PARIS NORD SITE BRETONNEAU APHP     | Centre Hospitalier Régional (C.H.R.)                         |
| 920100047 | 750712184 | HU PARIS NORD SITE LOUIS MOURIER APHP  | Centre Hospitalier Régional (C.H.R.)                         |
| 920100062 | 750712184 | HU PARIS OUEST SITE CELTON APHP        | Centre Hospitalier Régional (C.H.R.)                         |
| 750803447 | 750712184 | HU PARIS OUEST SITE G POMPIDOU APHP    | Centre Hospitalier Régional (C.H.R.)                         |
| 750100216 | 750712184 | HU PARIS OUEST SITE VAUGIRARD APHP     | Centre Hospitalier Régional (C.H.R.)                         |
| 930100037 | 750712184 | HU PARIS SITE AVICENNE APHP            | Centre Hospitalier Régional (C.H.R.)                         |
| 930100045 | 750712184 | HU PARIS SITE JEAN VERDIER APHP        | Centre Hospitalier Régional (C.H.R.)                         |
| 920100054 | 750712184 | HU PARIS SITE RAYMOND POINCARE APHP    | Centre Hospitalier Régional (C.H.R.)                         |
| 930100011 | 750712184 | HU PARIS SSTDENIS SITE MURET APHP      | Centre Hospitalier Régional (C.H.R.)                         |
| 920100021 | 750712184 | HU PARIS SUD SITE ANTOINE BECLERE APHP | Centre Hospitalier Régional (C.H.R.)                         |
| 940100043 | 750712184 | HU PARIS SUD SITE KREMLIN BICETRE APHP | Centre Hospitalier Régional (C.H.R.)                         |
| 940100068 | 750712184 | HU PARIS SUD SITE PAUL BROUSSE APHP    | Centre Hospitalier Régional (C.H.R.)                         |
| 750100125 | 750712184 | HU PITIE SALPETRIERE APHP              | Centre Hospitalier Régional (C.H.R.)                         |
| 940100035 | 750712184 | HU PITIE SALPETRIERE- CHARLE FOIX APHP | Centre Hospitalier Régional (C.H.R.)                         |
| 750803454 | 750712184 | HU ROBERT DEBRE APHP                   | Centre Hospitalier Régional (C.H.R.)                         |
| 750100067 | 750712184 | HU SAINT LOUIS SITE FERNAND WIDAL APHP | Centre Hospitalier Régional (C.H.R.)                         |
| 750100042 | 750712184 | HU SAINT LOUIS SITE LARIBOISIERE APHP  | Centre Hospitalier Régional (C.H.R.)                         |
| 750100075 | 750712184 | HU SAINT LOUIS SITE SAINT LOUIS APHP   | Centre Hospitalier Régional (C.H.R.)                         |
| 750019028 | 750712184 | IFAS-IFSI DE L'HÔPITAL SAINT ANTOINE   | Ecoles Formant aux Professions Sanitaires                    |
| 750803306 | 750712184 | IFSI AP-HP DE L'HÔPITAL ROTHSCHILD     | Ecoles Formant aux Professions Sanitaires                    |
| 750803371 | 750712184 | IFSI AP-HP DE L'HÔPITAL TENON          | Ecoles Formant aux Professions Sanitaires                    |
| 920008059 | 750712184 | IFSI AP-HP DU CH AMBROISE PARÉ         | Ecoles Formant aux Professions Sanitaires                    |
| 930811294 | 750712184 | IFSI AVICENNE (AP-HP)                  | Ecoles Formant aux Professions Sanitaires                    |
| 750008344 | 750712184 | IFSI COCHIN-LA ROCHEFOUCAULT (AP-HP)   | Ecoles Formant aux Professions Sanitaires                    |
| 940005788 | 750712184 | IFSI DE L'HÔPITAL BICÊTRE              | Ecoles Formant aux Professions Sanitaires                    |
| 930011408 | 750712184 | IFSI DE L'HÔPITAL JEAN VERDIER         | Ecoles Formant aux Professions Sanitaires                    |
| 920008158 | 750712184 | IFSI DE L'HÔPITAL LOUIS MOURIER        | Ecoles Formant aux Professions Sanitaires                    |
| 920008208 | 750712184 | IFSI DE L'HÔPITAL RAYMOND POINCARÉ     | Ecoles Formant aux Professions Sanitaires                    |
| 750018988 | 750712184 | IFSI DE L'HÔPITAL SAINT LOUIS          | Ecoles Formant aux Professions Sanitaires                    |
| 920008109 | 750712184 | IFSI DU CH ANTOINE BÉCLÈRE             | Ecoles Formant aux Professions Sanitaires                    |
| 750047524 | 750712184 | IFSI DU GROUPE HOSP. PITIÉ SALPÉTRIÈRE | Ecoles Formant aux Professions Sanitaires                    |
| 940005739 | 750712184 | IFSI HENRI MONDOR                      | Ecoles Formant aux Professions Sanitaires                    |
| 940005838 | 750712184 | IFSI HÔPITAL PAUL BROUSSE              | Ecoles Formant aux Professions Sanitaires                    |
| 750803058 | 750712184 | IFSI-IFAS DU CH BICHAT-CLAUDE BERNARD  | Ecoles Formant aux Professions Sanitaires                    |
| 920009958 | 750712184 | INSTITUT FORMATION SOINS INFIRMIERS    | Ecoles Formant aux Professions Sanitaires                    |
| 750100315 | 750712184 | MAISON MEDICALE ROCHEFOUCAULD APHP     | Etablissement de Soins Longue Durée                          |
| 830025268 | 750712184 | MAS SAN SALVADOUR                      | Maison d'Accueil Spécialisée (M.A.S.)                        |
| 940018021 | 750712184 | SERVICE CENTRAL DES AMBULANCES         | Centre Hospitalier Régional (C.H.R.)                         |
| 750100356 | 750712184 | UNITE GERONTIQUE LA COLLEGIALE AP HP   | Etablissement de Soins Longue Durée                          |
| 750058745 | 750712184 | USLD EST PARISIEN SITE ROTHSCHILD APHP | Etablissement de Soins Longue Durée                          |
| 940022833 | 750712184 | USLD HENRI MONDOR CHENEVRIER APHP      | Etablissement de Soins Longue Durée                          |
| 910021963 | 750712184 | USLD HENRI MONDOR SITE CLEMENCEAU APHP | Etablissement de Soins Longue Durée                          |
| 940022825 | 750712184 | USLD HENRI MONDOR SITE E ROUX APHP     | Etablissement de Soins Longue Durée                          |
| 910021955 | 750712184 | USLD HENRI MONDOR SITE JOFFRE APHP     | Etablissement de Soins Longue Durée                          |
| 750058729 | 750712184 | USLD PARIS CENTRE SITE BROCA APHP      | Etablissement de Soins Longue Durée                          |
| 750058695 | 750712184 | USLD PARIS IDF SITE STE PERINE APHP    | Etablissement de Soins Longue Durée                          |
| 750058687 | 750712184 | USLD PARIS NORD SITE BRETONNEAU APHP   | Etablissement de Soins Longue Durée                          |
| 920030178 | 750712184 | USLD PARIS NORD SITE L MOURIER APHP    | Etablissement de Soins Longue Durée                          |
| 920030160 | 750712184 | USLD PARIS OUEST SITE CELTON           | Etablissement de Soins Longue Durée                          |
| 750058703 | 750712184 | USLD PARIS OUEST SITE VAUGIRARD APHP   | Etablissement de Soins Longue Durée                          |
| 930026752 | 750712184 | USLD PARIS SSTDENIS SITE MURET APHP    | Etablissement de Soins Longue Durée                          |
| 940022858 | 750712184 | USLD PARIS SUD SITE PAUL BROUSSE APHP  | Etablissement de Soins Longue Durée                          |
| 750058737 | 750712184 | USLD PITIE SALPETRIERE APHP            | Etablissement de Soins Longue Durée                          |
| 750058711 | 750712184 | USLD SAINT LOUIS SITE F WIDAL APHP     | Etablissement de Soins Longue Durée                          |
| 940022841 | 750712184 | USLD SALPETRIERE SITE CHARLE FOIX      | Etablissement de Soins Longue Durée                          |
| 750019069 | 750712184 | ÉCOLE DE PUERICULTURE DU CH TROUSSEAU  | Ecoles Formant aux Professions Sanitaires                    |
| 750803199 | 750712184 | ÉCOLE SAGES FEMMES PARIS-BAUDELOCQUE   | Ecoles Formant aux Professions Sanitaires                    |

Quand le FINESS juridique change (changement de propriétaire par ex.), le FINESS établissement ne change pas:

| structureet | nofinesset | nofinessej | rs                          | rslongue                                           | complrs | compldistrib | numvoie | typvoie | voie          | compvoie | lieuditbp | commune | departement | libdepartement | ligneacheminement | telephone | telecopie | categetab | libcategetab                                           | categagretab | libcategagretab                                         | siret       | codeape | codemft | libmft                             | codesph | libsph | dateouv    | dateautor  | datemaj    | numuai |
|-------------|------------|------------|-----------------------------|----------------------------------------------------|---------|--------------|---------|---------|---------------|----------|-----------|---------|-------------|----------------|-------------------|-----------|-----------|-----------|--------------------------------------------------------|--------------|---------------------------------------------------------|-------------|---------|---------|------------------------------------|---------|--------|------------|------------|------------|--------|
| structureet | **750057184**  | **750803660**  | FOYER DE VIE OHT VERSAILLES | FOYER DE VIE DE L OEUVRE HOSPITALITE DU TRAVAIL    |         |              | 52      | AV      | DE VERSAILLES |          |           | 116     | 75          | PARIS          | 75016 PARIS       | 142880234 | 184169814 | 382       | Foyer de Vie pour Adultes Handicapés                   | 4301         | Etab. et Services d'Hébergement pour Adultes Handicapés | 77568891400016 |         | 8       | Président du Conseil Départemental |         |        | 01/01/2015 | 19/03/2010 | 14/02/2018 |        |
| structureet | **750057184**  | **750721029**  | E.A.N.M MOULIN VERT         | ETABLISSEMENT D'ACCUEIL NON MEDICALISE MOULIN VERT |         |              | 52      | AV      | DE VERSAILLES |          |           | 116     | 75          | PARIS          | 75016 PARIS       | 142880234 | 184169814 | 449       | Etab.Accueil Non Médicalisé pour personnes handicapées | 4301         | Etab. et Services d'Hébergement pour Adultes Handicapés |             |         | 8       | Président du Conseil Départemental |         |        | 01/01/2015 | 19/03/2010 | 12/05/2021 |        |


Le FINESS peut être abordé en lisant le guide methodo MCO du PMSI: ![guide methodo MCO](https://www.atih.sante.fr/sites/default/files/public/content/3735/guide_methodo_2020_6_bis_version_bo.pdf):

> "Un établissement de santé, identifié par un numéro FINESS juridique, est constitué d’un ou de plusieurs établissements dits géographiques, identifiés chacun par un numéro FINESS géographique. Le recueil PMSI doit-être réalisé sur la base du FINESS juridique pour les établissements publics et sur la base du FINESS géographique pour les établissements privés."

> "Il doit être enregistré dans le **RUM** :<br />
> * le numéro FINESS de l’entité juridique pour les établissements de santé publics ;
> * le numéro FINESS de l’entité géographique pour les établissements de santé privés."

Le PMSI fait apparaître une 3ieme notion: le "numéro FINESS d’inscription **e-PMSI**".

> "Il est indispensable que le numéro FINESS enregistré dans le **RSS** soit identique à celui avec lequel l’établissement est inscrit sur la plateforme e-PMSI pour pouvoir effectuer correctement la transmission des informations."

=> les FINESS sont essentiels pour identifier un établissement de santé.
=> le "FINESS PMSI" est le seul des 3 FINESS qui soit "interopérable". Malheureusement, sauf erreur, il n'est pas public.

# Description des données d'entrées

* 95936 ES
* 243 catégories
* CSV Etalab en 2 parties: adresses, codes et geoloc

![CSV FINESS en 2 parties](res/termino-00.png)

* geoloc:
  * **LAMBERT_93**
  * UTM_N20
  * UTM_N21
  * UTM_N22
  * UTM_S38
  * UTM_S40

=> pas de WGS84

=> pas d'alignement sur la BAN (https://www.data.gouv.fr/fr/datasets/base-adresse-nationale/)

=> nombreux 'reuses' (cf. page etalab)

Etude de cas autour de:


> "AGEPS APHP DE NANTERRE":
> structureet;**920000122**;750712184;AGEPS APHP DE NANTERRE;AGENCE GENERALE DES EQUIPEMENTS ET PRODUITS DE SANTE APHP;;;13;R;LAVOISIER;;;050;92;HAUTS DE SEINE;92000 NANTERRE;0146691313;0146691201;101;Centre Hospitalier Régional (C.H.R.);1101;Centres Hospitaliers Régionaux;26750045200979;;02;Autorité Ministérielle;1;Etablissement public de santé;1904-04-04;1904-04-04;2016-08-04;
> ...
> geolocalisation;**920000122**;**639484.6;6866492.7**;1,ATLASANTE,100,IGN,BD_ADRESSE,V2.2,**LAMBERT_93**;2021-03-08



# Modélisation

```
┌──────────┐                ┌────────────┐
│          │                │            │
│  DONNEES │                │            │
│          │    Modélisation│            │
│          │                │    OWL     │
│  ENTREE  │   ──────────►  │            │
│          │                │            │
│          │                │            │
│          │                │            │
└──┬───────┘                └──────┬─────┘
   │                               │
   │  │                            │  inspire le modèle
   │  │                            │ ───────────────┐
   │  │                            ▼ ─────────────┐ │
   │  │                      ┌────────────┐       | │
   │  │                      │            │       | │
   │  │                      │            │       | │
   │  │                      │   OMOP     │       | │
   │  │     Modélisation     │            │       | │
   │  └───────────────────►  │            │  ◄────┘ │
   │                         │            │         │
   │                         └────────────┘         │
   │                                                │
   │                                                │
   │                           ┌──────────┐         │
   │                           │          │         │
   │                           │          │         │
   │                           │   Autre  │         │
   └────────────────────────►  │          │ ◄───────┘
                               │          │
                               │          │
                               └──────────┘
```

# OWL

250 classes <br />
95936 instances <br />
2 089 068 triplets <br />

![FINESS dans Protégé](res/termino-01.png)

Focus sur "AGEPS Nanterre":

![AGEPS dans Protégé](res/termino-02-02.png)

geocodage WGS84
interop: vCard, GeoSPARQL

=> compatibilité à tester!

https://www.openstreetmap.org/?mlat=48.8955950496936&mlon=2.174611719896399#map=19/48.8955950496936/2.174611719896399&layers=A

![AGEPS dans OSM](res/termino-03-03.png)

Interopérable ?!

FINESS dans Bioportal:

![FINESS dans Bioportal](res/termino-01-00.png)

FINESS dans Webprotégé:

![FINESS dans WebProtégé](res/termino-06-01.png)

![FINESS dans WebProtégé](res/termino-06-02.png)

![FINESS dans WebProtégé](res/termino-06-03.png)

# OMOP

## CARE_SITE.csv

| care_site_id | care_site_name         | place_of_service_concept_id | location_id | care_site_source_value | place_of_service_source_value |
|--------------|------------------------|-----------------------------|-------------|------------------------|-------------------------------|
| 868787826    | AGEPS APHP DE NANTERRE | 0                           | 868901273   |                        |                               |
| 868630923    | AGEPS APHP DE NANTERRE | 0                           | 868901273   |                        |                               |

```
SELECT * FROM care_site c WHERE c.care_site_id IN (868787826, 868630923)
```

![CARE_SITE dans PGAdmin](res/pgadmin-01-01.PNG)

## LOCATION.csv

location.address_1 VARCHAR(50) -> VARCHAR(100):

```
ALTER TABLE location
ALTER COLUMN address_1 SET DATA TYPE VARCHAR(100);
```

| location_id | address_1      | address_2 | city     | state | zip   | county | country | location_source_value | latitude          | longitude         |
|-------------|----------------|-----------|----------|-------|-------|--------|---------|-----------------------|-------------------|-------------------|
| 868901273   | 13 R LAVOISIER |           | NANTERRE |       | 92000 |        | FR      |                       | 48.89559504969364 | 2.174611719896399 |

```
SELECT * FROM location l WHERE l.location_id = 868901273
```

![LOCATION dans PGAdmin](res/pgadmin-05-01.PNG)

## PROVIDER.csv

| provider_id | provider_name          | care_site_id | specialty_concept_id | gender_concept_id | specialty_source_concept_id | gender_source_concept_id |
|-------------|------------------------|--------------|----------------------|-------------------|-----------------------------|--------------------------|
| 870942391   | AGEPS APHP DE NANTERRE | 868787826    | 0                    | 0                 | 0                           | 0                        |
| 869409288   | AGEPS APHP DE NANTERRE | 868630923    | 0                    | 0                 | 0                           | 0                        |


```
SELECT * FROM provider p WHERE p.care_site_id IN (868787826, 868630923)
```

![PROVIDER dans PGAdmin](res/pgadmin-04-01.PNG)

## FACT_RELATIONSHIP.csv

FACT_RELATIONSHIP: Hiérarchie des CARE_SITE et Lien CARE_SITE / CONCEPT:

| domain_concept_id_1 | fact_id_1 | domain_concept_id_2 | fact_id_2 | relationship_concept_id |
|---------------------|-----------|---------------------|-----------|-------------------------|
| 57                  | 868787826 | 58                  | 868040340 | 44819237                |
| 57                  | 868787826 | 57                  | 868601775 | 46233688                |
| 57                  | 868601775 | 57                  | 868787826 | 46233689                |
| 57                  | 868630923 | 58                  | 868395316 | 44819237                |
| 57                  | 868630923 | 57                  | 868601775 | 46233688                |
| 57                  | 868601775 | 57                  | 868630923 | 46233689                |

avec:

```
57 Care site
58 Type Concept
44819237 Non-standard to type concept map (OMOP)
46233688 Care Site is part of Care Site
46233689 Care Site contains Care Site
```

```
SELECT * FROM fact_relationship f WHERE f.fact_id_1 in (868787826, 868630923)
UNION
SELECT * FROM fact_relationship f WHERE f.fact_id_2 in (868787826, 868630923)
```

![FACT_RELATIONSHIP dans PGAdmin](res/pgadmin-02-01.PNG)

=> découverte de 2 relations vers 2 concepts: 868040340 et 868395316

Puis arborescence des CONCEPT / CONCEPT_CLASS / ... / VOCABULARY :

## CONCEPT.csv

| concept_id | concept_name           | domain_id | vocabulary_id | concept_class_id | standard_concept | concept_code | valid_start_date | valid_end_date | invalid_reason |
|------------|------------------------|-----------|---------------|------------------|------------------|--------------|------------------|----------------|----------------|
| 868395316  | AGEPS APHP DE NANTERRE | Care site | FINESS        | FINESS_101       | 8                | 920000122    | 01/01/1970       | 31/12/2099     |                |
| 868040340  | AGEPS APHP DE NANTERRE | Care site | FINESS        | FINESS_101       | 8                | 750712184    | 01/01/1970       | 31/12/2099     |                |

où:

```
"920000122": FINESS ET (Etablissement) de AGEPS NANTERRE
"750712184": FINESS EJ (Entité juridique) de AGEPS NANTERRE
```

```
SELECT * FROM concept c WHERE c.concept_id in (868040340, 868395316)
```

![CONCEPT dans PGAdmin](res/pgadmin-03-01.PNG)

![Détails AGEPS ET dans Athena](res/termino-04.png)

L'AGEPS est un CHR (hiérarchie reconstruite et navigable):

![Détails C.H.R dans Athena](res/termino-05.png)

## Liste des FINESS ET

```
select
  c.concept_id,
  c.concept_code as finesset,
  c.concept_name,
  ca.care_site_id,
  ca.care_site_name,
  ca.location_id,
  l.address_1,
  l.address_2,
  l.zip,
  l.city,
  l.latitude,
  l.longitude,
  p.provider_id
from
  concept_relationship cr,
  concept c,
  fact_relationship fr,
  care_site ca,
  location l,
  provider p
where
      cr.concept_id_1 = c.concept_id
  and cr.concept_id_2 = 99999986 /*Finess ET*/
  and cr.relationship_id = 'Is a'
  and fr.fact_id_2 = c.concept_id
  and fr.domain_concept_id_2 = 58 /*Type Concept*/
  and fr.relationship_concept_id = 44819237 /*Non-standard to type concept map (OMOP)*/
  and ca.care_site_id = fr.fact_id_1
  and l.location_id = ca.location_id
  and p.care_site_id = ca.care_site_id
```

##  Liste des pharmacies

```
select
  c.concept_id,
  c.concept_code as finesset,
  c.concept_name,
  ca.care_site_id,
  ca.care_site_name,
  ca.location_id,
  l.address_1,
  l.address_2,
  l.zip,
  l.city,
  l.latitude,
  l.longitude,
  p.provider_id
from
  concept_relationship cr1,
  concept_relationship cr2,
  concept c,
  fact_relationship fr,
  care_site ca,
  location l,
  provider p
where
      cr1.concept_id_1 = c.concept_id
  and cr1.concept_id_2 = 99999986 /*Finess ET*/
  and cr1.relationship_id = 'Is a'
  and cr2.concept_id_1 = c.concept_id
  and cr2.concept_id_2 in (868495235 /*Pharmacie d'officine*/, 868146852 /*Propharmacie*/, 868074503 /*Pharmacie mutualiste*/)
  and cr2.relationship_id = 'Is a'
  and fr.fact_id_2 = c.concept_id
  and fr.domain_concept_id_2 = 58 /*Type Concept*/
  and fr.relationship_concept_id = 44819237 /*Non-standard to type concept map (OMOP)*/
  and ca.care_site_id = fr.fact_id_1
  and l.location_id = ca.location_id
  and p.care_site_id = ca.care_site_id
```

# FHIR

## [LOCATION](https://www.hl7.org/fhir/location.html)

'Details and position information for a physical place where services are provided and resources and participants may be stored, found, contained, or accommodated.'

| OMOP CDM | field                 | required | type         | description                                                                                                                    | FHIR                        |
|----------|-----------------------|----------|--------------|--------------------------------------------------------------------------------------------------------------------------------|-----------------------------|
| LOCATION | location_id           | Yes      | INTEGER      | A unique identifier for each geographic location.                                                                              | location.id                 |
| LOCATION | address_1             | No       | VARCHAR(50)  | The address field 1, typically used for the street address, as it appears in the source data.                                  | location.address.line[0]    |
| LOCATION | address_2             | No       | VARCHAR(50)  | The address field 2, typically used for additional detail such as buildings, suites, floors, as it appears in the source data. | location.address.line[1]    |
| LOCATION | city                  | No       | VARCHAR(50)  | The city field as it appears in the source data.                                                                               | location.address.city       |
| LOCATION | state                 | No       | VARCHAR(2)   | The state field as it appears in the source data.                                                                              | location.address.state      |
| LOCATION | zip                   | No       | VARCHAR(9)   | The zip or postal code.                                                                                                        | location.address.postalCode |
| LOCATION | county                | No       | VARCHAR(20)  | The county.                                                                                                                    | location.address.district   |
| LOCATION | country               | No       | VARCHAR(100) | The country                                                                                                                    | location.address.country    |
| LOCATION | location_source_value | No       | VARCHAR(50)  | The verbatim information that is used to uniquely identify the location as it appears in the source data.                      |                             |
| LOCATION | latitude              | No       | FLOAT        | The geocoded latitude                                                                                                          | location.address.latitude   |
| LOCATION | longitude             | No       | FLOAT        | The geocoded longitude                                                                                                         | location.address.longitude  |

[Location.managingOrganization](https://www.hl7.org/fhir/location-definitions.html#Location.managingOrganization)

## [ORGANIZATION](https://www.hl7.org/fhir/organization.html)

'A formally or informally recognized grouping of people or organizations formed for the purpose of achieving some form of collective action. Includes companies, institutions, corporations, departments, community groups, healthcare practice groups, payer/insurer, etc.'

## [HEALTHCARESERVICE](https://www.hl7.org/fhir/healthcareservice.html)

'The HealthcareService resource is used to describe a single healthcare service or category of services that are provided by an organization at a location.
The location of the services could be virtual, as with telemedicine services.'

# Présentation de GeoFriness

GeoFriness fournit des données géographiques ('Geo') libres ('Fri') et interopérables autour du Finess.

## Enjeu du GeoFriness

Le Finess (acronyme de "fichier national des établissements sanitaires et sociaux"), tenu par la Drees (acronyme de "direction de la Recherche, des Études, de l'Évaluation et des Statistiques").
Les données du Finess sont fournies en Opendata sur [data.gouv.fr](https://www.data.gouv.fr/en/datasets/finess-extraction-du-fichier-des-etablissements/) sous la forme d'un CSV en 2 parties contenant à date 98337 établissements.

La deuxième partie du CSV contient les géolocalisations généralement formulées en LANBERT fournies par le projet [Atlasanté](https://catalogue.atlasante.fr/geonetwork/srv/fre/catalog.search#/home):

```
structureet;980503007;980502991;CPTS SUD;COMMUNAUTE PROFESSIONNELLE TERRITORIAL DE SANTE;;;;;;;BP 31;606;9F;MAYOTTE;97620 CHIRONGUI;0639695555;;604;Communautés professionnelles territoriales de santé (CPTS);2103;Autres structures d'exercice libéral;;;99;Indéterminé;;;2023-11-28;2023-03-20;2023-12-20;
geolocalisation;010000024;870215.7;6571590.5;1,ATLASANTE,100,IGN,BD_ADRESSE,V2.2,LAMBERT_93;2024-05-13
```

A l'image des données propres au Finess (numéros, adresses et types d'etablissement), cinq remarques s'imposent concernant les données géographiques:
- elles sont fournies dans un format non interopérables (CSV, bizarre qui plus est);
- elles n'alimentent pas d'autres systèmes de données ou de cartographie opensource connus, au premier rang desquels wikipedia, wikidata ou OpenStreeMap;
- elles alimentent [des cartes qui ne sont pas tenues à jour (updates en 2021)](https://www.atlasante.fr/accueil/cartes-et-outils/nos-cartes);
- elles alimentent des [jeux de données qui ne sont pas libres](https://www.atlasante.fr/accueil/cartes-et-outils/nos-cartes-a-acces-restreint);
- ce qui a pour incidence de les laisser sans rapport avec les usages qu'on peut espèrer tirer de tel croisement d'outils et d'informations.

On constate dès lors un déphasage entre les informations fournies par l'Etat et celles mises à disposition du grand public:
- OpenStreetMap ou wikidata contiennent en propre des informations sur les établissements de santé qui sont parcelaires;
- les informations géolocalisées fournies par l'Etat sous exploitées par des outils payants qui les complètent et les corrigent masquant ainsi la faible qualité de l'information primaire (adresses non alignées sur le [Fantoir](https://www.collectivites-locales.gouv.fr/competences/le-fichier-des-voies-et-lieux-dits-fantoir), coordonnées sous forme de points uniques là où des informations périmètriques ou des géolocalisations de points d'accès seraient plus utiles que des centroïdes...).

En normalisant les géolocaisations proposées par l'Etat sur du WGS84 reconnu par les sercices grand public et en fournissant des ressources interopérables (GeoJSON, OWL requétable en SparQL), GeoFriness répond à ce triple enjeu:
- rendre utiles ces données publiques;
- permettre/préparer leur intégration systématique dans les bases de données publiques;
- permettre l'évaluation de la qualité des données publiques en agilisant leur confrontation à d'autres données et en exposant leurs limites.


# Finess dans jOSM

## Modélisation GeoJSON du Finess dans GeoFriness

Petit à petit, on identifie des properties OpenStreetMap existantes et sémantiquement équivalentes aux propriétés Finess d'origine. On n'introduit de nouvelles propriétés que si elles n'existent vraiment pas.
On utilise:
[Keys](https://wiki.openstreetmap.org/wiki/FR:Key:type:FR:FINESS)
[Taginfo / keys](https://taginfo.openstreetmap.org/keys)
[Tags for healthcare](https://wiki.openstreetmap.org/wiki/Category:Tag_descriptions_for_group_%22health%22)
[Comment cartographier](https://wiki.openstreetmap.org/wiki/FR:Comment_cartographier_un...)
[Comment cartographier (santé)](https://wiki.openstreetmap.org/wiki/FR:Comment_cartographier_un_(sant%C3%A9))
[Amenity: social, clinic, hospital](https://wiki.openstreetmap.org/wiki/Key:social_facility#:~:text=The%20social_facility%3Afor%20%3D*%20tag%20is%20used%20to%20describe%20the,primarily%20served%20by%20the%20facility.)

Par exemple:

```json
  "type:FR:FINESS" : "355",
  "ref:FR:FINESS" : "010000024",
  "ref:FR:SIRET" : "26010004500012",
  "ref:INSEE" : "01451",
  "postal_code" : "01440",
  "postal_code:country" : "FR"
```

Pour un établissement de santé (ES), à date, ca donne:

```json
{
  "type" : "Feature",
  "geometry" : {
    "type" : "Point",
    "coordinates" : [ 5.2085961326419605, 46.22274483202441 ]
  },
  "properties" : {
    "name" : "CH DE FLEYRIAT",
    "type:FR:FINESS" : "355",
    "ref:FR:FINESS" : "010000024",
    "ref:FR:SIRET" : "26010004500012",
    "ref:INSEE" : "01451",
    "postal_code" : "01440",
    "postal_code:country" : "FR",
    "structureet" : "structureet",
    "nofinesset" : "010000024",
    "nofinessej" : "010780054",
    "rs" : "CH DE FLEYRIAT",
    "rslongue" : "CENTRE HOSPITALIER DE BOURG-EN-BRESSE FLEYRIAT",
    "numvoie" : "900",
    "typvoie" : "RTE",
    "voie" : "DE PARIS",
    "commune" : "451",
    "departement" : "01",
    "libdepartement" : "AIN",
    "ligneacheminement" : "01440 VIRIAT",
    "telephone" : "0474454647",
    "telecopie" : "0474454114",
    "categetab" : "355",
    "libcategetab" : "Centre Hospitalier (C.H.)",
    "categagretab" : "1102",
    "libcategagretab" : "Centres Hospitaliers",
    "siret" : "26010004500012",
    "codeape" : "8610Z",
    "codemft" : "03",
    "libmft" : "ARS établissements Publics de santé dotation globale",
    "codesph" : "1",
    "libsph" : "Etablissement public de santé",
    "dateouv" : "1979-02-13",
    "dateautor" : "1979-02-13",
    "datemaj" : "2020-02-04",
    "hash" : "9ea719aa3d32192a8b180862d43d933f5fd7fd064036ca74e308fb9d2a0c3751"
  }
}
```

## Installer jOSM

```
set JAVA_HOME=...\jdk-21.0.2\
set PATH=.;%JAVA_HOME%bin;%PATH%
java -jar josm-18604.jar
```

## Configurer jOSM

Edition > Préférences > Coloriage > + > "Finess" et sélectionner le fichier "finess.mapcss" fourni par GeoFriness:

![FINESS: MapCSS dans jOSM](res/josm-00.png)

![FINESS: MapCSS dans jOSM](res/josm-01.png)

Imagerie > OpenStreetMap Carto (Standard)

![FINESS: Aperçu](res/josm-02.png)

Fichier > Ouvrir > sélectioner le fichier finess.geojson

## Utiliser jOSM

Filtrer > + > categetab=604 > cocher le check "I"
(NB: categetab=604 correspond aux CPTS)

![FINESS: filtre](res/josm-03.png)

![FINESS: résultat](res/josm-04.png)


# Autres outils OSM

## Overpass

![Overpass](https://overpass-turbo.eu/)

```
/*
Overpass query : loop for FINESS tags around Paris
*/

area[name="Paris"];
nwr["type:FR:FINESS"](area);
out center;
```
## Outils et Pratiques de codage dans OpenStreeMap, compléments de modélisation à envisager

[Sophox 'SparQL'](https://wiki.openstreetmap.org/wiki/Sophox)

[Créer des cartes](http://umap.openstreetmap.fr/fr/)

[jOSM et Fantoir](https://lists.openstreetmap.org/pipermail/talk-fr/2021-May/103903.html)
[Topo versus Fantoir](https://forum.openstreetmap.fr/t/le-fichier-topo-remplace-enfin-fantoir-dans-bano/21310)

[Outil de controle qualité (osmose)](https://osmose.openstreetmap.fr/fr/map/#loc=13/48.59699/2.08628/0/27)
[Outil de controle qualité (pifomètre)](https://bano.openstreetmap.fr/pifometre/?insee=92002) 

[OSM et BigData: planet.osm](https://wiki.openstreetmap.org/wiki/Planet.osm#Processing_the_file)

Une [source de données GeoJSON (région Bretagne, canton de Corte...) ](https://github.com/gregoiredavid/france-geojson/blob/master/regions/bretagne/region-bretagne.geojson)

## Outils de mise à jour en masse de wikidata

[OpenRefine](https://www.wikidata.org/wiki/Wikidata:Tools/OpenRefine)
[QuickStatements](https://www.wikidata.org/wiki/Help:QuickStatements)

[Tuto1](https://media.ed.ac.uk/media/How+to+use+QuickStatements+-+a+tool+to+bulk+upload+data+onto+Wikidata./1_bbmmepx3)
[Tuto2](https://www.youtube.com/watch?v=FtpyPruId7I)

# Ontologie géographique et GeoSPARQL

## Introduction à GeoSPARQL

Requête GeoSparql avec [Qlever](https://qlever.cs.uni-freiburg.de/osm-planet/q5ryEs)

Les Pays-Bas ont fait d'énormes efforts autour des ontologies géographiques et de GeoSparql.
On pourra regarder notamment cette [repo github](https://github.com/CLARIAH/wp4-queries-geo?tab=readme-ov-file).

Pour prendre un des exemples proposé de bout en bout:

Aller sur [yasgui.org](https://yasgui.org/)
Spécifier le endpoint: https://api.druid.datalegend.net/datasets/nlgis/cshapes/services/cshapes/sparql
Entrer la requête GeoSparql suivante:

```sparql
#+ summary: Countries with border changes since 1988. Best viewed as map in https://yasgui.org
#+ tags:
#+      - CShapes
#+      - Correlates of War
#+      - Gleditsch and Ward (1999)
#+      - http://nils.weidmann.ws/projects/cshapes.html
#+ endpoint: https://api.druid.datalegend.net/datasets/nlgis/cshapes/services/cshapes/sparql


PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX iisgv: <https://iisg.amsterdam/vocab/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?wkt (concat('hsv,', ?age) as ?wktColor) (concat(?name, ' (',?isoCode,'): ', ?startDate) as ?wktLabel) WHERE {  
  ?country rdf:type iisgv:CountrySlice .
  ?country iisgv:isoName ?name .
  ?country iisgv:isoNumber ?isoCode .
  ?country geo:hasGeometry/geo:asWKT ?wkt .
  ?country iisgv:cowStart ?startDate .
  OPTIONAL{?country iisgv:wgStart ?startDate} .
  BIND(replace(str(?startDate),"(\\d+)-\\d*-\\d*", "$1") as ?year) .
  BIND(xsd:float(xsd:integer(?year) - 1988)/30 as ?age) .
  BIND(xsd:dateTime(?startDate) as ?startDateTime) .
  FILTER(xsd:dateTime(?startDateTime) >= '1988-01-01T00:00:00Z'^^xsd:dateTime &&
         xsd:dateTime(?startDateTime) <= '2017-12-12T23:59:59Z'^^xsd:dateTime) .
}
LIMIT 100
```

## Kadaster (cadastre neerlandais)

gros effort autour de geosparql + ontologie
geoyasgui > devient un plugin geo et geo-3D de yasgui porté par TripleDB (payant)

https://data.labs.kadaster.nl/dst/kkg/browser?resource=http%3A%2F%2Fwww.opengis.net%2Font%2Fgeosparql%23asGML


## Requetage Sparql de l'ontologie Wikidata autour de la santé

[Finess connus de Wikidata](https://query.wikidata.org/#%23defaultView%3AMap%0ASELECT%20DISTINCT%20%3Finstitution%20%3FinstitutionLabel%20%3Fgeoloc%20WHERE%20%7B%0A%20%20%0A%20%20%3Finstitution%20wdt%3AP4058%20%3Ffiness%20.%0A%20%20%0A%20%20OPTIONAL%20%7B%20%3Finstitution%20wdt%3AP625%20%3Fgeoloc%20%7D%20.%20%23%20get%20the%20geolocation%20of%20the%20institution%0A%20%20%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cfr%22.%20%7D%0A%0A%7D)


[La même requête en passant par les statements](https://query.wikidata.org/#%23defaultView%3AMap%0ASELECT%20DISTINCT%20%3Finstitution%20%3FinstitutionLabel%20%3Ffiness%20%3Ffinessid%20%3Fgeoloc%20%3Fgeo%20WHERE%20%7B%0A%20%20%0A%20%20%3Finstitution%20p%3AP4058%20%3Ffiness%20.%0A%20%20%3Ffiness%20ps%3AP4058%20%3Ffinessid%20.%0A%20%20%0A%20%20OPTIONAL%20%7B%20%3Finstitution%20p%3AP625%20%3Fgeoloc%20.%20%0A%20%20%20%20%20%20%20%20%20%20%20%20%3Fgeoloc%20ps%3AP625%20%3Fgeo%20.%20%7D%20%23%20get%20the%20geolocation%20of%20the%20institution%0A%20%20%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cfr%22.%20%7D%0A%0A%7D) (meilleur pour progresser ultérieurement dans le graphe Wikidata)


La requête:
```sparql
#defaultView:Map
PREFIX schema: <http://schema.org/>
PREFIX hist: <http://wikiba.se/history/ontology#>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
SELECT DISTINCT ?institution ?institutionLabel ?finess ?finessid ?geoloc ?geo ?date WHERE {
  
  ?institution p:P4058 ?finess .
  ?finess ps:P4058 ?finessid .
  ?institution schema:dateModified ?date .
 
  
  OPTIONAL { ?institution p:P625 ?geoloc .  
              ?geoloc ps:P625 ?geo .
           } # get the geolocation of the institution
  
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],fr". }

}
```

Le résultat reporté sur la carte:

![FINESS: wikidata](res/wikidata-00.png)

On obtient 15K+ communes "ayant un Finess" (comprendre: commune ayant un ES ayant un Finess), ce qui pose de pbs sémantiques et de complétude (15K versus 100K) à regarder.

## Modélisation OWL du Finess dans GeoFriness

On utilise au maximum les prefixes, ontologies, propriétés existantes.

En particulier on utilise [OpenGIS](res/ontology.png) et [geosparql](res/geosparql-ontology.png).


[wikidata](https://www.wikidata.org/wiki/Special:Search?search=finess&ns120=1&fulltext=Search+for+a+property&fulltext=Search) donne des idées de propriétés "interopérables" à défaut de "réutilisables":

```
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>

# on retient notamment:

wdt:P625 : Coordinate location (https://www.wikidata.org/wiki/Property:P625, WGS84)
wdt:P4058 : FINESS (https://www.wikidata.org/wiki/Property:P4058)
wdt:P3215 : SIRET (https://www.wikidata.org/wiki/Property:P3215)
wdt:P402: OpenStreetMap relation Id (https://www.wikidata.org/wiki/Property:P402)
```

https://github.com/galbiston/geosparql-fuseki/blob/master/geosparql_test_27700.rdf
+ ?country geo:hasGeometry/geo:asWKT ?wkt . :

<rdf:RDF xmlns:geo="http://www.opengis.net/ont/geosparql#"
         xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
         xmlns:ex="http://example.org/Schema#"
         xmlns:feat="http://example.org/Feature#"
         xmlns:geom="http://example.org/Geometry#"
         xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
         xmlns:sf="http://www.opengis.net/ont/sf#"
         xmlns:owl="http://www.w3.org/2002/07/owl#">

### Ontologie GIS: asWKT et POINT et spécifation du CRS

    <!-- Point C -->
    <ex:PlaceOfInterest rdf:about="http://example.org/Feature#C">
        <ex:hasExactGeometry rdf:resource="http://example.org/Geometry#PointC"/>
        <ex:name rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PlaceC</ex:name>
    </ex:PlaceOfInterest>
    <sf:Point rdf:about="http://example.org/Geometry#PointC">
        <geo:asWKT rdf:datatype="http://www.opengis.net/ont/geosparql#wktLiteral"><![CDATA[<http://www.opengis.net/def/crs/EPSG/0/27700> POINT(30 20)]]></geo:asWKT>
    </sf:Point>

"<http://www.opengis.net/def/crs/EPSG/0/4326> POINT (0.0 0.0)"^^<http://www.opengis.net/ont/geosparql#wktLiteral>

### WGS84

GeoJSON n'utilise que WGS84.

L'URI de WGS84/CRS84 est: http://www.opengis.net/def/crs/OGC/1.3/CRS84
son urn: urn:ogc:def:crs:OGC:1.3:CRS84
pas d'OID

https://www.w3.org/2015/spatial/wiki/Coordinate_Reference_Systems

GeoJSON uses the same default as GeoSPARQL: if no CRS is specified, coordinates are assumed to be WGS84 with reversed axes.

In the WKT representation of geometry, GeoSPARQL uses the concept of a default CRS: “The URI <http://www.opengis.net/def/crs/OGC/1.3/CRS84> shall be assumed as the spatial reference system for geo:wktLiterals that do not specify an explicit spatial reference system URI.”

The URI http://www.opengis.net/def/crs/OGC/1.3/CRS84 denotes WGS84 with the order longitude, latitude.

https://ext.eurocontrol.int/aixm_confluence/display/ACG/Coordinate+Reference+System

The examples below show the encoding of the same Airport Reference Point (ARP) with different CRS applied. In the first example (OGC:1.3:CRS84), the longitude value comes first, then the latitude. In the second example (EPSG:4326), the latitude value comes first and then the longitude.


PREFIX sf: <http://www.opengis.net/ont/sf#>
http://www.opengis.net/ont/sf#Point

## geosparql avec Fuseki

geosparql extension for Jena est initialement un [projet à part](https://github.com/galbiston/geosparql-fuseki/blob/master/) intégré dans Jena Fuseki après.

```
docker run --rm -v datas:/fuseki/databases/ -p3030:3030 -it fuseki
```

Surtout:

```
cd C:\Apps\jena-fuseki
java -jar jena-fuseki-geosparql-5.0.0.jar -rf "finess.ttl" -i
```

Interroger le endpoint SparQL exposé à l'[http://127.0.0.1:3030/ds/sparql](http://127.0.0.1:3030/ds/sparql) avec une requête du type:


Lister les ES autour de Corte:

```sparql
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX org: <http://www.w3.org/ns/org#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT ?label ?fGeom ?fWKT ?addrr ?pcode
WHERE {
    ?fGeom geo:asWKT ?fWKT .
    ?fGeom rdfs:label ?label .
    ?fGeom org:siteAddress ?addr .
    ?addr vcard:hasAddress [ vcard:street-address ?addrr;  vcard:postal-code ?pcode] .
FILTER (geof:sfWithin(?fWKT, '''
        <http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON((8.910515345588937 42.34573619787828, 9.225342298576274 42.344721199064, 9.224312330354941 42.19686424711812, 8.905365504482274 42.199153334870765, 8.910515345588937 42.34573619787828))      
'''^^geo:wktLiteral))
}
```

La réponse:

|ES|URI|
|---|---|
|"CH CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0000038>|
|"EHPAD U SERENU"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0003107>|
|"CMP ADULTES ET ENFANTS DE CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0004196>|
|"CENTRE DE PLANIFICATION FAMILIALE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0005243>|
|"CONSULTATIONS DE NOURISSONS"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0005250>|
|"SAAD ADMR CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0006340>|
|"RES AUTO CORSE AIDE A LA PERSONNE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0006399>|
|"SELEURL PHARMACIE DE CALACUCCIA"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0002315>|
|"SAAD ADMR VENACO"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0006464>|
|"CENTRE DE SOINS DENTAIRE MUTUALISTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0001549>|
|"PHARMACIE PLACE PADOUE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0002687>|
|"USLD DE CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0005722>|
|"LBM 2A-2B / SITE DE CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0005763>|
|"SELAS PHARMACIE MANZI"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0002760>|
|"BAPU (BUREAU AIDE PSYCHO UNIV)"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0005821>|
|"SELARL PHARMACIE DU ROND-POINT CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0002885>|
|"ANTENNE SMUR DE CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0004907>|
|"SAAD CORSE AIDE A LA PERSONNE  ( CAP )"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0005987>|
|"PHARMACIE DU VENACAIS"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0002976>|
|"SERV. MEDECINE PREVENTIVE UNIV. CORSE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0006001>|
|"UNITE D'AUTODIALYSE DE CORTE"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0004071>|
|"SAAD AMAPA ( CORTE)"|<http://drees.solidarites-sante.gouv.fr/terminologies/finess/2B0006142>|


### Utilisation de geofriness avec yasgui, exemples de requêtes

Aller sur:
https://yasgui.triply.cc/


Entrer le endpoint Sparql:
https://geofiness-xmlt3unxbq-od.a.run.app/ds/sparql

#### ES dans le canton de Corte

Entrer la requête:
```sparql
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX org: <http://www.w3.org/ns/org#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT ?label ?fGeom ?CRS84 ?addrr ?pcode
WHERE {
    ?fGeom geo:asWKT ?fWKT .
    ?fGeom rdfs:label ?label .
    ?fGeom org:siteAddress ?addr .
    ?addr vcard:hasAddress [ vcard:street-address ?addrr;  vcard:postal-code ?pcode] .
    
FILTER (geof:sfWithin(?fWKT, '''
        <http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON((8.910515345588937 42.34573619787828, 9.225342298576274 42.344721199064, 9.224312330354941 42.19686424711812, 8.905365504482274 42.199153334870765, 8.910515345588937 42.34573619787828))      
'''^^geo:wktLiteral)) .
  BIND(STRDT(REPLACE(str(?fWKT), "<http\\:\\/\\/www\\.opengis\\.net\\/def\\/crs\\/OGC\\/1\\.3\\/CRS84> POINT \\((.+) (.+)\\)", "POINT($1 $2)", "i"), geo:wktLiteral) AS ?CRS84)
}
```

#### ES dans le canton de Corte, matérialisation du canton

Plus joli, avec contours du canton de Corte:

```sparql
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX org: <http://www.w3.org/ns/org#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT * WHERE
{ 
{
SELECT ?eventLabel ?CRS84
WHERE {
    ?fGeom geo:asWKT ?fWKT .
    ?fGeom rdfs:label ?eventLabel .
    ?fGeom org:siteAddress ?addr .
    ?addr vcard:hasAddress [ vcard:street-address ?addrr;  vcard:postal-code ?pcode] .
FILTER (geof:sfWithin(?fWKT, '''
        <http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON((8.9265 42.2547,8.9351 42.2575,8.9765 42.2409,8.9918 42.2429,9.0081 42.2613,9.0307 42.2807,9.0332 42.2921,9.0725 42.3123,9.0859 42.3135,9.1119 42.3361,9.1446 42.3441,9.1722 42.3201,9.1942 42.32,9.1877 42.2945,9.1969 42.2871,9.2228 42.2894,9.2347 42.2736,9.225 42.2631,9.2328 42.2496,9.2311 42.2371,9.2456 42.228,9.2004 42.2241,9.1881 42.2165,9.175 42.2007,9.189 42.1966,9.1943 42.1837,9.2193 42.163,9.1998 42.1481,9.181 42.1417,9.1517 42.104,9.1335 42.0909,9.1247 42.105,9.1044 42.1166,9.0951 42.1148,9.0757 42.1292,9.0728 42.1536,9.0658 42.1658,9.049 42.1704,9.0518 42.1873,9.0459 42.2072,9.022 42.2032,8.9933 42.2158,8.9772 42.231,8.9452 42.235,8.9265 42.2547))   
'''^^geo:wktLiteral)) .
  BIND(STRDT(REPLACE(str(?fWKT), "<http\\:\\/\\/www\\.opengis\\.net\\/def\\/crs\\/OGC\\/1\\.3\\/CRS84> POINT \\((.+) (.+)\\)", "POINT($1 $2)", "i"), geo:wktLiteral) AS ?CRS84)
}
}
UNION
{
    SELECT ("canton de Corte" as ?eventLabel) ("POLYGON((8.9265 42.2547,8.9351 42.2575,8.9765 42.2409,8.9918 42.2429,9.0081 42.2613,9.0307 42.2807,9.0332 42.2921,9.0725 42.3123,9.0859 42.3135,9.1119 42.3361,9.1446 42.3441,9.1722 42.3201,9.1942 42.32,9.1877 42.2945,9.1969 42.2871,9.2228 42.2894,9.2347 42.2736,9.225 42.2631,9.2328 42.2496,9.2311 42.2371,9.2456 42.228,9.2004 42.2241,9.1881 42.2165,9.175 42.2007,9.189 42.1966,9.1943 42.1837,9.2193 42.163,9.1998 42.1481,9.181 42.1417,9.1517 42.104,9.1335 42.0909,9.1247 42.105,9.1044 42.1166,9.0951 42.1148,9.0757 42.1292,9.0728 42.1536,9.0658 42.1658,9.049 42.1704,9.0518 42.1873,9.0459 42.2072,9.022 42.2032,8.9933 42.2158,8.9772 42.231,8.9452 42.235,8.9265 42.2547))"^^geo:wktLiteral as ?CRS84)
WHERE {
}
}
}
```

![résultat](res/yasgui-00.png)


#### Pharmacies sur Saint-Malo 1

```sparql
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX org: <http://www.w3.org/ns/org#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT * WHERE
{ 
{
SELECT ?eventLabel ?CRS84
WHERE {
    # CPTS: ?fGeom rdf:type <http://drees.solidarites-sante.gouv.fr/terminologies/finess/604> .
    ?fGeom rdf:type <http://drees.solidarites-sante.gouv.fr/terminologies/finess/620> .
    ?fGeom geo:asWKT ?fWKT .
    ?fGeom rdfs:label ?eventLabel .
    ?fGeom org:siteAddress ?addr .
    ?addr vcard:hasAddress [ vcard:street-address ?addrr;  vcard:postal-code ?pcode] .
FILTER (geof:sfWithin(?fWKT, '''
        <http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON((-1.8564 48.625,-1.8746 48.6107,-1.8626 48.602,-1.8673 48.5866,-1.8869 48.5942,-1.9089 48.5935,-1.9071 48.6074,-1.9248 48.615,-1.9998 48.6406,-2.0215 48.6387,-2.0314 48.6502,-1.9966 48.6614,-1.9705 48.6865,-1.9485 48.6828,-1.9377 48.6955,-1.9058 48.6903,-1.8876 48.7003,-1.8491 48.7017,-1.8367 48.6795,-1.8617 48.6681,-1.8721 48.6463,-1.8564 48.625))   
'''^^geo:wktLiteral)) .
  BIND(STRDT(REPLACE(str(?fWKT), "<http\\:\\/\\/www\\.opengis\\.net\\/def\\/crs\\/OGC\\/1\\.3\\/CRS84> POINT \\((.+) (.+)\\)", "POINT($1 $2)", "i"), geo:wktLiteral) AS ?CRS84)
}
}
UNION
{
    SELECT ("Saint-Malo 1" as ?eventLabel) ("POLYGON((-1.8564 48.625,-1.8746 48.6107,-1.8626 48.602,-1.8673 48.5866,-1.8869 48.5942,-1.9089 48.5935,-1.9071 48.6074,-1.9248 48.615,-1.9998 48.6406,-2.0215 48.6387,-2.0314 48.6502,-1.9966 48.6614,-1.9705 48.6865,-1.9485 48.6828,-1.9377 48.6955,-1.9058 48.6903,-1.8876 48.7003,-1.8491 48.7017,-1.8367 48.6795,-1.8617 48.6681,-1.8721 48.6463,-1.8564 48.625))"^^geo:wktLiteral as ?CRS84)
WHERE {
}
}
}
```

![résultat](res/saint-malo-1-pharmacies.png)
![résultat](res/saint-malo-1-pharmacies-2.png)
![résultat](res/saint-malo-1-pharmacies-3.png)

#### CPTS en Bretagne


```sparql
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX org: <http://www.w3.org/ns/org#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT * WHERE
{ 
{
SELECT ?eventLabel ?CRS84
WHERE {
    ?fGeom rdf:type <http://drees.solidarites-sante.gouv.fr/terminologies/finess/604> .
    ?fGeom geo:asWKT ?fWKT .
    ?fGeom rdfs:label ?eventLabel .
    ?fGeom org:siteAddress ?addr .
    ?addr vcard:hasAddress [ vcard:street-address ?addrr;  vcard:postal-code ?pcode] .
FILTER (geof:sfWithin(?fWKT, '''
        <http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON((-5.157009854142506 48.967274881015165, -1.4166980720986047 48.967274881015165, -1.4166980720986047 47.28791663312708, -5.157009854142506 47.28791663312708, -5.157009854142506 48.967274881015165))   
'''^^geo:wktLiteral)) .
  BIND(STRDT(REPLACE(str(?fWKT), "<http\\:\\/\\/www\\.opengis\\.net\\/def\\/crs\\/OGC\\/1\\.3\\/CRS84> POINT \\((.+) (.+)\\)", "POINT($1 $2)", "i"), geo:wktLiteral) AS ?CRS84)
}
}
UNION
{
    SELECT ("Bretagne" as ?eventLabel) ("POLYGON((-5.157009854142506 48.967274881015165, -1.4166980720986047 48.967274881015165, -1.4166980720986047 47.28791663312708, -5.157009854142506 47.28791663312708, -5.157009854142506 48.967274881015165))"^^geo:wktLiteral as ?CRS84)
WHERE {
}
}
}
```

![résultat](res/cpts-bretagne-1.png)
![résultat](res/cpts-bretagne-2.png)
![résultat](res/cpts-bretagne-3.png)

#### CPTS en Auvergne Rhone Alpes

```sparql
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX org: <http://www.w3.org/ns/org#>
PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>

SELECT * WHERE
{ 
{
SELECT ?eventLabel ?CRS84
WHERE {
    ?fGeom rdf:type <http://drees.solidarites-sante.gouv.fr/terminologies/finess/604> .
    ?fGeom geo:asWKT ?fWKT .
    ?fGeom rdfs:label ?eventLabel .
    ?fGeom org:siteAddress ?addr .
    ?addr vcard:hasAddress [ vcard:street-address ?addrr;  vcard:postal-code ?pcode] .
FILTER (geof:sfWithin(?fWKT, '''
        <http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON((1.8853437442779275 46.8979130506044, 7.21371267597201 46.8979130506044, 7.21371267597201 44.04829877902603, 1.8853437442779275 44.04829877902603, 1.8853437442779275 46.8979130506044))   
'''^^geo:wktLiteral)) .
  BIND(STRDT(REPLACE(str(?fWKT), "<http\\:\\/\\/www\\.opengis\\.net\\/def\\/crs\\/OGC\\/1\\.3\\/CRS84> POINT \\((.+) (.+)\\)", "POINT($1 $2)", "i"), geo:wktLiteral) AS ?CRS84)
}
}
UNION
{
    SELECT ("ARA" as ?eventLabel) ("POLYGON((1.8853437442779275 46.8979130506044, 7.21371267597201 46.8979130506044, 7.21371267597201 44.04829877902603, 1.8853437442779275 44.04829877902603, 1.8853437442779275 46.8979130506044))"^^geo:wktLiteral as ?CRS84)
WHERE {
}
}
}
```

![résultat](res/cpts-ara-1.png)
![résultat](res/cpts-ara-2.png)
![résultat](res/cpts-ara-3.png)

A mettre en rapport avec la [carte des CPTS de Auvergne Rhone Alpes](https://carto.atlasante.fr/1/cpts_ARA.map)
![carte Altasante / CPTS / ARA](res/cpts-ara-atlasante.png)

### Python
[import contextily as ctx](https://www.pierreauclair.org/blog/velibs2.html)


# GeoFriness dans QGIS

## Installer QGIS

Aller sur le [site web de Qgis](https://qgis.org/download/) et télécharger la version qui convient au poste cible (c'est gros)

## Charger GeoFriness
Explorateur > "C:\ (OS)" > naviguer_vers_geofriness > dat > osm > finess.zip > finess.geojson
Double clique sur finess.geojson

Les champs sont:

|Champ|Type|
|---|---|
|name|String|
|type:FR:FINESS|String|
|ref:FR:FINESS|String|
|ref:FR:SIRET|String|
|ref:INSEE|String|
|postal_code|String|
|postal_code:country|String|
|structureet|String|
|nofinesset|String|
|nofinessej|String|
|rs|String|
|rslongue|String|
|complrs|String|
|compldistrib|String|
|numvoie|String|
|typvoie|String|
|voie|String|
|compvoie|String|
|lieuditbp|String|
|commune|String|
|departement|String|
|libdepartement|String|
|ligneacheminement|String|
|telephone|String|
|telecopie|String|
|categetab|String|
|libcategetab|String|
|categagretab|String|
|libcategagretab|String|
|siret|String|
|codeape|String|
|codemft|String|
|libmft|String|
|codesph|String|
|libsph|String|
|dateouv|Date|
|dateautor|Date|
|datemaj|Date|
|numuai|String|
|hash|String|
|auxiliary_storage_labeling_show|Integer|


Explorateur > "XYZ Tiles" > doubler cliquer sur "OpenStreetMap"

Explorateur > clic droit sur "WFS / OGC API - Features" > "Nouvelle connexion"
"[Ajouter](https://geoservices.ign.fr/documentation/services/utilisation-sig/tutoriel-qgis/wfs)" > Nom : "IGN" > URL : "https://data.geopf.fr/wfs/ows?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities" > OK

Sélectionner "Données Base Adresse Nationale", une couche avec le nom technique "BAN.DATA.GOUV:ban" apparait.

Les champs sont:

|Champ|Type|
|---|---|
|id|xsd:string|
|id_fantoir|xsd:string|
|numero|xsd:string|
|rep|xsd:string|
|nom_voie|xsd:string|
|code_postal|xsd:string|
|code_insee|xsd:string|
|nom_commune|xsd:string|
|code_insee_ancienne_commune|xsd:string|
|nom_ancienne_commune|xsd:string|
|x|xsd:string|
|y|xsd:string|
|lon|xsd:double|
|lat|xsd:double|
|type_position|xsd:string|
|alias|xsd:string|
|nom_ld|xsd:string|
|libelle_acheminement|xsd:string|
|nom_afnor|xsd:string|
|source_position|xsd:string|
|source_nom_voie|xsd:string|
|certification_commune|xsd:string|
|cad_parcelles|xsd:string|

Sélectionner "Etablissements de santé".

Les champs sont:

|Champ|Type|
|---|---|
|nature|xsd:string|
|designations|xsd:string|
|etat_de_l_objet|xsd:string|
|urgence|xsd:string|
|identifiant_gestionnaire|xsd:string|
|num_finess|xsd:string|
|toponyme|xsd:string|
|adresse|xsd:string|
|telephone|xsd:string|
|libcateget|xsd:string|
|picto|xsd:string|
|acheminement|xsd:string|
nb_pai  xsd:int

Sélectionner "BDTOPO_V3:batiment" / "BDTOPO : Bâtiments".

Les champs sont:

|Champ|Type|
|---|---|
|cleabs|xsd:string|
|nature|xsd:string|
|usage_1|xsd:string|
|usage_2|xsd:string|
|construction_legere|xsd:boolean|
|etat_de_l_objet|xsd:string|
|date_creation|xsd:dateTIme|
|date_modification|xsd:dateTIme|
|date_d_apparition|xsd:date|
|date_de_confirmation|xsd:date|
|sources|xsd:string|
|identifiants_sources|xsd:string|
|methode_d_acquisition_planimetrique|xsd:string|
|methode_d_acquisition_altimetrique|xsd:string|
|precision_planimetrique|xsd:double|
|precision_altimetrique|xsd:double|
|nombre_de_logements|xsd:int|
|nombre_d_etages|xsd:int|
|materiaux_des_murs|xsd:string|
|materiaux_de_la_toiture|xsd:string|
|hauteur|xsd:double|
|altitude_minimale_sol|xsd:double|
|altitude_minimale_toit|xsd:double|
|altitude_maximale_toit|xsd:double|
|altitude_maximale_sol|xsd:double|
|origine_du_batiment|xsd:string|
|appariement_fichiers_fonciers|xsd:string|
|identifiants_rnb|xsd:string|

=> BDTOPO est alignée sur le RNB (champ "identifiants_rnb")


Ajouter la [BAN éditée par l'IGN](https://data.geopf.fr/annexes/ressources/wms-v/adresse.xml) au format WMS.

Une couche "BAN édition 2024-06-24" apparait.

Ajouter le RNB: "Vector Tiles" > "https://rnb-api.beta.gouv.fr/api/alpha/tiles/{x}/{y}/{z}.pbf" , spécifier le niveau de zoom 19-20 et nommer la couche "RNB".

![résultat](res/qgis-ban-finess.png)

RNB (en noir), BAN (en rouge, adresses non certifiées), Finess (croix rouge sur cercle en fond blanc, 3 en tout sur l'aperçu), BDTOPO batiments (polygones verts):
![résultat](res/qgis-ban-rnb-finess.png)

Important!:

"Préférences" > "Options" > "Sources de données" > "Comportement des tables d'attributs" > "Montrer les entités visibles sur la carte"

Puis:
"finess" > clic droit > "Ouvrir la table d'attributs"

Sinon il faut faire:
"finess" > clic droit > "Ouvrir la table d'attributs"
> "Montrer toutes les entités" > "Ne montrer les entités visibles sur la carte"
Mais dans le cas de grandes sources de données ou de sources de données en ligne, QGis se fait en carafe (ou c'est lent)...

![résultat](res/qgis-finess-attributs.png)

Faire de même sur les autres couches (notamment "Etablissements de santé, peu d'ES y sont)!

"Vue" > "Afficher les infobulles"
"finess" > clic droit > "Propriétés" > "Infobulles" > infobulle HTML > "<span><div>[%"name"%] ([%"type:FR:FINESS"%], [%"ref:FR:FINESS"%], [%"numvoie"%] [%"typvoie"%] [%"voie"%] [%"ligneacheminement"%])</div></span>"


# Interopérabilité syntaxique
Bilan des champs listés ci-dessus à faire
Voir Normes La Poste, DGFIP, Etalab, INSEE, internationales...
"ref:FR:FINESS" et "ref:FR:RNB", versus "identifiants_rnb"


# Ressources

[Font-GIS](https://viglino.github.io/font-gis/)
INSEE Base Publique des Equipements / BPE2023
[Icones OSM](https://wiki.openstreetmap.org/wiki/Category:OSM_Carto_icons)
[Icones OSM](https://wiki.openstreetmap.org/wiki/OpenStreetMap_Carto/Symbols#Healthcare)

# Finess: Règles de controles qualité

Une même adresse ne peut pas être écrite différemment ("AV 9 SEPTEMBRE", "AV DU 9 SEPTEMBRE", "ALL DU 9 SEPTEMBRE"):
![résultat](res/finess-qualite-001.png)

Plusieurs Finess a une même position géographique: OK

Mais plusieurs adresses postales radicalement différentes à une même position géographique: KO

![résultat](res/finess-qualite-002.png)

Si on regarde l'intégralité du fichier (>1 adresse postale pour 1 même point géographique), on trouve 8085 points géographiques différents pour 23779 finesset à regarder plus en détail pour vraiment justifier que ces ES se trouvent ainsi correctement groupés ou non.
Ainsi l'exemple sur Corte: il est normal de trouver 2 ES à la meme geoloc mais l'adresse est KO avec une RTE dans un cas et une AVE dans l'autre).



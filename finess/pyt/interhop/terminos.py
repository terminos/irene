import pandas as pd

class Concept:
    def __init__(self, id, name, label_long):
        self.id = id
        self.name = name
        self.label_long = label_long

    def __eq__(self, other):
        """
        if not isinstance(other, Concept):
            return NotImplemented
        id_self = str(self.id) if not pd.isna(self.id) else 'nan'
        id_other = str(other.id) if not pd.isna(other.id) else 'nan'
        name_self = str(self.name) if not pd.isna(self.name) else 'nan'
        name_other = str(other.name) if not pd.isna(other.name) else 'nan'
        label_long_self = str(self.label_long) if not pd.isna(self.label_long) else 'nan'
        label_long_other = str(other.label_long) if not pd.isna(other.label_long) else 'nan'
        b = (   id_self == id_other
            and name_self == name_other
            and label_long_self == label_long_other
            )
        return (
            b
        )
        """
        return (self is other)
    
    def __repr__(self):
        return f"Concept(id='{self.id}', name='{self.name}', label_long='{self.label_long}')"


class Termino:
    def __init__(self, id, name, concepts=None):
        self.id = id
        self.name = name
        self.concepts = concepts if concepts is not None else []

    def __eq__(self, other):
        """
        if isinstance(other, Termino):
            # Comparer l'id, le nom et la liste de concepts
            return (self.id == other.id and
                    self.name == other.name and
                    self.concepts == other.concepts)
        return False
        """
        return (self is other)

    def setConcepts(self, ref_df):
        # Créer les Concepts pour ce Référentiel
        for _, row in ref_df.iterrows():
            concept = Concept(row["code"], row["label"], row["label"])
            self.concepts.append(concept)

    def __repr__(self):
        return f"Termino(id='{self.id}', name='{self.name}', concepts={len(self.concepts)})"


class UnitéProduction:
    def __init__(self, id, name, terminos=None):
        self.id = id
        self.name = name
        self.terminos = terminos if terminos is not None else []

    def setTerminos(self, up_df):
        # Filtrer les Référentiels pour cette Unité de Production
        for ref in up_df["ref_alias"].drop_duplicates():
            ref_df = up_df[up_df["ref_alias"] == ref]
            oid = ref_df["oid"].iloc[0]
            refo = Termino(oid, ref)
            refo.setConcepts(ref_df)
            self.terminos.append(refo)

    def __eq__(self, other):
        """
        if isinstance(other, UnitéProduction):
            # Comparer l'id, le nom et la liste de terminos
            return (self.id == other.id and
                    self.name == other.name and
                    self.terminos == other.terminos)
        return False
        """
        return (self is other)

    def __repr__(self):
        return f"UnitéProduction(id='{self.id}', name='{self.name}', terminos={len(self.terminos)})"


class PersonneMorale:
    def __init__(self, id, name, ups=None):
        self.id = id
        self.name = name
        self.ups = ups if ups is not None else []

    def setUnitéProductions(self, pm_df):
        # Filtrer les Unités de Production pour cette Personne Morale
        for up in pm_df["up"].drop_duplicates():
            up_df = pm_df[pm_df["up"] == up]
            upo = UnitéProduction(up, up)
            upo.setTerminos(up_df)
            self.ups.append(upo)

    def __eq__(self, other):
        """
        if isinstance(other, PersonneMorale):
            # Comparer l'id, le nom et la liste des UP
            return (self.id == other.id and
                    self.name == other.name and
                    self.ups == other.ups)
        return False
        """
        return (self is other)

    def __repr__(self):
        return f"PersonneMorale(id='{self.id}', name='{self.name}', ups={len(self.ups)})"


class TerminosDAOFileImpl:
    def __init__(self, file_path):
        self.file_path = file_path
        try:
            self.dataframe = pd.read_csv(file_path, dtype=str, sep=";")
        except Exception as e:
            raise ValueError(f"Erreur lors du chargement du fichier {file_path}: {e}")
        
        # Vérification des colonnes nécessaires
        required_columns = {"pm","up","ref","ref_alias","oid","code","label","label_long"}
        # required_columns = {"pm","up","ref","code","label","label_long","structureet"}
        
        if not required_columns.issubset(self.dataframe.columns):
            raise ValueError(f"Le fichier doit contenir les colonnes : {', '.join(required_columns)}")
        
        self._parse()
    
    def _parse(self):
        # On commence le parsing au niveau des Personnes Morales
        pm_df = self.dataframe
        if pm_df.empty:
            return
        self.pms = []
        for pm in pm_df["pm"].drop_duplicates():
            pm_df_filtered = pm_df[pm_df["pm"] == pm]
            pmo = PersonneMorale(pm, pm)
            pmo.setUnitéProductions(pm_df_filtered)
            self.pms.append(pmo)


    def get_up_by_pm(self, pm):
        """
        Recherche les Unités de Production associées à une Personne Morale (pm).
        :param pm: La Personne Morale pour laquelle on recherche les Unités de Production.
        :return: Une liste des instances de UnitéProduction associées à cette Personne Morale.
        """

        # Parcourir les Personnes Morales
        for pm_instance in self.pms:
            if pm_instance.id == pm:
                # Retourner les Unités de Production associées à cette Personne Morale
                return pm_instance.ups

        return


    def get_terminos_by_up(self, up):
        """
        Recherche les Terminos associés à un Unité de Production (up).
        :param up: L'Unité de Production pour laquelle on recherche les Terminos.
        :return: Une liste des instances de Termino associées à cet Unité de Production.
        """

        # Parcourir les Personnes Morales
        for pm in self.pms:
            # Parcourir les Unités de Production de chaque Personne Morale
            for up_instance in pm.ups:
                if up_instance.id == up:
                    # Retourner les Terminos associés à cet Unité de Production
                    return up_instance.terminos

        return


    def get_concepts_by_termino(self, ref):
        """
        Recherche les concepts associés à un Termino donné par sa référence (ref).
        :param ref: La référence du Termino à rechercher.
        :return: Une liste d'instances de Concept associées à ce Termino.
        """
        # Parcourir les Personnes Morales
        for pm in self.pms:
            # Parcourir les Unités de Production de chaque Personne Morale
            for up in pm.ups:
                # Parcourir les Terminos de chaque Unité de Production
                for t in up.terminos:
                    if t.id == ref:  # Vérifier si c'est le bon Termino
                        return t.concepts
        return

    def get_pm_by_name(self, name):

        for pm in self.pms:
            if pm.name == name:
                return pm

        return None  
    
    def get_termino_by_oid(self, oid):

        for pm in self.pms:
            for up in pm.ups:
                for t in up.terminos:
                    if t.id == oid:
                        return t

        return None  

    def get_concept_by_code(self, code, termino=None):
        """
        Recherche un concept par son code dans l'arborescence des Personnes Morales, Unités de Production et Terminos.
        :param code: Le code du concept à rechercher.
        :param termino: L'instance de Termino dans laquelle chercher. Si None, recherche globalement.
        :return: Une instance de Concept si trouvée, None sinon.
        """
        # Si un Termino est spécifié, rechercher uniquement dans ce Termino
        if termino:
            for pm in self.pms:
                for up in pm.ups:
                    for t in up.terminos:
                        if t == termino:  # Vérifier si le Termino correspond
                            # Recherche du Concept dans ce Termino
                            for concept in t.concepts:
                                if concept.id == code:
                                    return concept
        else:
            # Recherche globale dans l'arborescence
            for pm in self.pms:
                for up in pm.ups:
                    for t in up.terminos:
                        for concept in t.concepts:
                            if concept.id == code:
                                return concept

        return None  # Si le Concept n'est pas trouvé


    def get_termino_from_concept(self, concept):
        """
        Récupère l'instance de Termino à laquelle appartient un concept donné.
        :param concept: Une instance de Concept.
        :return: Une instance de Termino si trouvée, None sinon.
        """
        if not isinstance(concept, Concept):
            raise ValueError("Le paramètre concept doit être une instance de la classe Concept.")
        
        # Parcourir les Terminos pour trouver celui qui contient le concept
        for pm in self.pms:
            for up in pm.ups:
                for termino in up.terminos:
                    #if not isinstance(termino, Termino):
                    #    continue  # Assurer que nous parcourons bien des instances de Termino
                    for c in termino.concepts:
                        # print(f"Comparing {c} with {concept}")  # Déboguer la comparaison
                        if c == concept:  # Utilise __eq__ pour comparer
                            return termino
        
        return None

    def get_up_from_termino(self, termino):
        """
        Récupère l'instance de UnitéProduction à laquelle appartient un Termino donné.
        :param termino: Une instance de Termino.
        :return: Une instance de UnitéProduction si trouvée, None sinon.
        """
        if not isinstance(termino, Termino):
            raise ValueError("Le paramètre termino doit être une instance de la classe Termino.")
        
        for pm in self.pms:
            for up in pm.ups:
                for t in up.terminos:
                    if t == termino:  # Utilise __eq__ pour comparer
                        return up
        
        return None

    def get_pm_from_up(self, up):
        """
        Récupère l'instance de PersonneMorale à laquelle appartient une UnitéProduction donnée.
        :param up: Une instance de UnitéProduction.
        :return: Une instance de PersonneMorale si trouvée, None sinon.
        """
        if not isinstance(up, UnitéProduction):
            raise ValueError("Le paramètre up doit être une instance de la classe UnitéProduction.")
        
        # Parcourir les Personnes Morales pour trouver celle qui contient l'UnitéProduction
        for pm in self.pms:
            for u in pm.ups:
                if u == up:  # Utilise __eq__ pour comparer
                    return pm
        
        return None


class DAOFactory:
    @staticmethod
    def get_terminos_dao(file_path):
        return TerminosDAOFileImpl(file_path)


# Exemple d'utilisation
if __name__ == "__main__":
    pass

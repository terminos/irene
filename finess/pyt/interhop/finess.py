import requests
import time
from lxml import etree
import pandas as pd
import re
from .terminos import *

class FinessET:
    # Définition des constantes de contexte
    CONTEXT_RECHERCHE_SIMPLE = "rechercheSimple"
    CONTEXT_IDENTIFICATION = "identification"
    CONTEXT_AMF = "AMF"
    CONTEXT_DISCIPLINE = "discipline"
    CONTEXT_EML = "EML"
    CONTEXT_FORMATION = "formation"
    CONTEXT_AMM = "AMM"

    # Liste des contextes
    CONTEXTS = [
        CONTEXT_RECHERCHE_SIMPLE,
        CONTEXT_IDENTIFICATION,
        CONTEXT_AMF,
        CONTEXT_DISCIPLINE,
        CONTEXT_EML,
        CONTEXT_FORMATION,
        CONTEXT_AMM,
    ]

    def __init__(self, dataframe):
        """
        Initialise une instance de FinessET.
        :param dataframe: DataFrame contenant les données FinessET.
        """
        self.dataframe = dataframe

    def print(self, context=None):
        """
        Affiche le DataFrame complet ou un contexte spécifique.
        :param context: Le contexte à afficher (optionnel).
        """
        if context is None:
            print(self.dataframe)
        elif context in self.CONTEXTS:
            if context in self.dataframe.columns:
                print(self.dataframe[context].iloc[0])
            else:
                print(f"Contexte '{context}' non disponible dans les données.")
        else:
            print(f"Contexte '{context}' non reconnu.")

    def hasContext(self, context):
        """
        Vérifie si un contexte donné existe et n'est ni null ni vide.
        :param context: Le contexte à vérifier.
        :return: True si le contexte existe et n'est pas vide, False sinon.
        """
        if context not in self.CONTEXTS:
            return False

        if context not in self.dataframe.columns:
            return False

        context_df = self.dataframe[context].iloc[0]  # Récupère le DataFrame du contexte

        # Vérifie si le DataFrame du contexte est null ou vide
        if isinstance(context_df, pd.DataFrame):
            return not context_df.empty

        return False

    def getContext(self, context):
        """
        Vérifie si un contexte donné existe et n'est ni null ni vide.
        :param context: Le contexte à vérifier.
        :return: True si le contexte existe et n'est pas vide, False sinon.
        """
        if context not in self.CONTEXTS:
            return False

        if context not in self.dataframe.columns:
            return False

        context_df = self.dataframe[context].iloc[0]  # Récupère le DataFrame du contexte

        return context_df
    
    def getValue(self, context, key, position=None):
        """
        Récupère une valeur dans un contexte donné pour une clé donnée.
        :param context: Le contexte où chercher.
        :param key: La clé à chercher dans le contexte.
        :param position: La position dans le DataFrame (optionnelle).
        :return: La valeur correspondante, le DataFrame entier, ou None si non trouvé.
        """
        if context not in self.CONTEXTS:
            return None

        if context not in self.dataframe.columns:
            return None

        context_df = self.dataframe[context].iloc[0]  # Récupère le DataFrame du contexte

        if isinstance(context_df, pd.DataFrame):
            if key in context_df.columns:
                if position is not None:
                    if position < len(context_df):
                        return context_df[key].iloc[position]
                    else:
                        return None
                else:
                    return context_df[key]  # Retourne toute la colonne si position non spécifiée

        return None
    
    
    def hasValue(self, context, key, value):
        """
        Vérifie si un contexte donné contient une clé et une valeur spécifiques.
        :param context: Le contexte à vérifier.
        :param key: La clé à chercher dans le contexte.
        :param value: La valeur attendue.
        :return: True si le contexte contient la clé avec la valeur exacte, False sinon.
        """
        if context not in self.CONTEXTS:
            return False

        if context not in self.dataframe.columns:
            return False

        context_df = self.dataframe[context].iloc[0]  # Récupère le DataFrame du contexte

        if isinstance(context_df, pd.DataFrame):
            if key in context_df.columns:
                # Vérifie si une correspondance exacte existe
                return any(context_df[key] == value)

        return False


class AnalyseurFinessET:
    def __init__(self):
        # Remplacements dans la chaîne
        self.replacements = {
            "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">": "",
            "&eacute;": "é",
            "&egrave;": "è",
            "&nbsp;": "",
            "&gt;": "",
            "&deg;": "",
            "&ecirc;": "",
            #"&oldFiness=": "&amp;oldFiness=",
            "&agrave;": "à",
            "&": "&amp;",  # Doit être fait en dernier pour éviter les conflits
            # "&aidetitle=": "&amp;aidetitle=",
            "colspan=2": 'colspan="2"',
            "width = ": "width=",
            "styleTitre\"width": "styleTitre\" width"
        }

        self.dao = DAOFactory.get_terminos_dao("./interhop/ref/interhop_finess_attributes.csv")

    def extract_and_compare(self, string, oid):
        """
        Extrait l'ID et le label d'une chaîne du type '[DE000] Pas de déclaration' 
        et compare le label extrait avec celui du Concept trouvé par l'ID.
        :param string: La chaîne du type '[DE000] Pas de déclaration'.
        :return: True si le label extrait correspond au label du Concept, False sinon.
        """
        # Utilisation d'une expression régulière pour extraire l'ID et le label
        match = re.match(r"\[([A-Za-z0-9]+)\]\s*(.*)", string)
        
        if match:
            # Extraction de l'ID et du label
            concept_id = match.group(1)
            label_extracted = match.group(2)

            # Recherche du Concept par son code (ID)
            concept = self.dao.get_concept_by_code(concept_id, self.dao.get_termino_by_oid(oid))
            
            if concept:
                # and concept.name == label_extracted:
                return concept
            else:
                print(f"Concept avec l'ID -{concept_id}- non trouvé.")
                return None
        else:
            print("La chaîne n'est pas dans le format attendu.")
            return None

    def manipulReponse(self, response_text, context):
        """
        Dispatcher pour manipuler les réponses selon le contexte.
        :param response_text: Texte de la réponse HTTP.
        :param context: Contexte de la réponse (ex. 'rechercheSimple', 'identification', etc.).
        """ 

        if context == FinessET.CONTEXT_RECHERCHE_SIMPLE:
            return self.manipulReponseRechercheSimple(response_text)
        elif context == FinessET.CONTEXT_IDENTIFICATION:
            return self.manipulReponseIdentification(response_text)
        elif context == FinessET.CONTEXT_AMF:
            return self.manipulReponseAMF(response_text)
        elif context == FinessET.CONTEXT_DISCIPLINE:
            return self.manipulReponseDiscipline(response_text)
        elif context == FinessET.CONTEXT_EML:
            return self.manipulReponseEML(response_text)
        elif context == FinessET.CONTEXT_FORMATION:
            return self.manipulReponseFormation(response_text)
        elif context == FinessET.CONTEXT_AMM:
            return self.manipulReponseAMM(response_text)
        else:
            print(f"Contexte inconnu : {context}")

    def manipulReponseRechercheSimple(self, response_text):
        """Traitement de la réponse pour la recherche simple."""
        if response_text:
            print("Analyseur: traitement de la réponse POST (recherche simple)...")

            # Remplacements pour nettoyer le texte HTML
            """
            replacements = {
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">": "",
                "&eacute;": "é",
                "&egrave;": "è",
                "&nbsp;": "",
                "&gt;": "",
                "&deg;": "",
                "&ecirc;": "",
                "&aidetitle=": "&amp;aidetitle=",
                "&agrave;": "à",
                "&": "&amp;",
            }
            """

            for old, new in self.replacements.items():
                response_text = response_text.replace(old, new)

            # Convertir en XML
            parser = etree.HTMLParser()
            root = etree.fromstring(response_text, parser)

            # Sélectionner la table avec class="ecranET"
            table = root.xpath("//table[@class='ecranET']")
            if not table:
                raise ValueError("Table with class 'ecranET' not found.")

            table = table[0]

            # Récupérer les informations demandées
            def clean_text(element):
                if element is None:
                    return ""
                text = etree.tostring(element, method="text", encoding="unicode")
                text = re.sub(r"\s+", " ", text)  # Remplacer les retours à la ligne par des espaces
                return text.strip()

            # Raison sociale
            raison_sociale = clean_text(table.xpath(".//tr[1]/td[2]")[0])

            # Numéro FINESS
            numero_finess = clean_text(table.xpath(".//tr[1]/td[3]")[0])
            numero_finess = numero_finess.replace("N FINESS :", "").strip()

            # Adresse administrative
            adresse_raw = table.xpath(".//tr[2]/td[2]")[0]
            adresse_admin = clean_text(adresse_raw)
            adresse_admin = re.sub(r"<i>|</i>", "", adresse_admin)  # Retirer les balises <i>
            adresse_admin = re.sub(r"<br.*?>", " ", adresse_admin)  # Remplacer <br> par espace
            adresse_admin = re.sub(r"\s+", " ", adresse_admin).strip()  # Retirer espaces multiples

            # Numéro SIRET
            numero_siret = clean_text(table.xpath(".//tr[2]/td[3]")[0])
            numero_siret = numero_siret.replace("N SIRET :", "").strip()

            # Numéro de téléphone
            telephone = clean_text(table.xpath(".//tr[3]/td[1]")[0])
            telephone = telephone.replace("Tél :", "").strip()

            # Numéro de fax
            fax = clean_text(table.xpath(".//tr[3]/td[2]")[0])
            fax = fax.replace("Fax :", "").strip()

            # Retourner les informations sous forme de dictionnaire
            ret = {
                "Raison Sociale": [str(raison_sociale)],
                "FinessET": [str(numero_finess).replace(" ", "")],
                "Adresse Administrative": [str(adresse_admin)],
                "SIRET": [str(numero_siret).replace(" ", "")],
                "Téléphone": [str(telephone).replace(" ", "")],
                "Fax": [str(fax).replace(" ", "")]
            }
            df = pd.DataFrame(ret)
            #print(df)
            return df

            # print(response_text[:100])
        else:
            print("Analyseur: réponse POST vide ou erreur.")

    def manipulReponseIdentification(self, response_text):
        """Traitement de la réponse pour l'onglet Identification."""
        if response_text:
            print("Analyseur: traitement de l'onglet Identification...")
            """
            Analyse et extrait les données des tables/tr de la réponse XML pour l'onglet Identification.
            :param response_text: Texte de la réponse XML.
            """
            # Remplacements dans la chaîne
            """
            replacements = {
                "&eacute;": "é",
                "&egrave;": "è",
                "&nbsp;": "",
                "&gt;": "",
                "&deg;": "",
                "&ecirc;": "",
                "&oldFiness=": "&amp;oldFiness=",
                "&agrave;": "à",
                "&": "&amp;",  # Doit être fait en dernier pour éviter les conflits
                "colspan=2": 'colspan="2"',
            }
            """

            for old, new in self.replacements.items():
                response_text = response_text.replace(old, new)

            # Conversion en XML
            try:
                xml_bytes = response_text.encode('utf-8')
                root = etree.fromstring(xml_bytes)
            except etree.ParseError as e:
                print(f"Erreur de parsing XML : {e}")
                return

            # Recherche des tables et extraction des données
            key_mapping = {
                "N FINESS :": "FinessET",
                "Date d'ouverture :": "Date d'ouverture",
                "N FINESS de l'entité juridique de rattachement :": "FinessEJ",
                "Catégorie de l'établissement :": "Catégorie de l'établissement",
                "Statut juridique de l'EJ :": "Statut juridique de l'EJ",
                "Mode de tarification :": "Mode de tarification",
                "Participation au service public hospitalier :": "Participation au service public hospitalier",
                "Code APE :": "Code APE",
            }

            data = []
            for table in root.findall(".//table"):
                for tr in table.findall(".//tr"):
                    tds = tr.findall(".//td")
                    if len(tds) >= 2:
                        key = tds[0].text.strip() if tds[0].text else ""
                        value = " ".join(td.text.strip() for td in tds[1:] if td.text)
                        if key in key_mapping:
                            key = key_mapping[key]  # Remplacer par la clé correspondante
                        if key == "FinessET" and "SIRET :" in value:
                            # Extraire le SIRET et ajuster le FinessET
                            match = re.search(r"(.*?)N SIRET : (.+)", value)
                            if match:
                                finesset_value = match.group(1).strip()
                                siret_value = match.group(2).strip()
                                data.append(("FinessET", finesset_value.replace(" ", "")))
                                data.append(("SIRET", siret_value.replace(" ", "")))
                            else:
                                data.append((key, value))
                        else:
                            data.append((key, value))

            # Affichage des résultats
            # Créer le DataFrame
            df = pd.DataFrame(data, columns=["Clé", "Valeur"])

            # Transposer le DataFrame
            df_transposed = df.set_index("Clé").T
            print("truc")
            return df_transposed
        
        else:
            print("Analyseur: réponse vide ou erreur pour l'onglet Identification.")

    def manipulReponseAMF(self, response_text):
        """Traitement de la réponse pour l'onglet AMF."""
        if response_text:
            print("Analyseur: traitement de l'onglet AMF...")
            """
            Analyse et extrait les données des triplets (activité, modalité, forme) de la réponse XML pour l'onglet AMF.
            :param response_text: Texte de la réponse XML.
            :return: Un DataFrame pandas avec les colonnes ["activité", "modalité", "forme"].
            """
            # Remplacements dans la chaîne
            """
            replacements = {
                "&eacute;": "é",
                "&egrave;": "è",
                "&nbsp;": "",
                "&gt;": "",
                "&deg;": "",
                "&ecirc;": "",
                "&oldFiness=": "&amp;oldFiness=",
                "&agrave;": "à",
                "&": "&amp;",  # Doit être fait en dernier pour éviter les conflits
                "colspan=2": 'colspan="2"',
            }
            """

            for old, new in self.replacements.items():
                response_text = response_text.replace(old, new)

            # Conversion en XML
            try:
                xml_bytes = response_text.encode('utf-8')
                root = etree.fromstring(xml_bytes)
            except etree.ParseError as e:
                print(f"Erreur de parsing XML : {e}")
                return pd.DataFrame(columns=["activité", "modalité", "forme"])

            # Recherche de la table et extraction des données
            data = []
            table = root.xpath("//table")[0]
            if table is not None:
                rows = table.xpath(".//tr[position() > 1]")
                for tr in rows:
                    tds = tr.findall(".//td")
                    if len(tds) == 3:  # Chaque ligne doit avoir exactement 3 colonnes
                        #row = [self.extract_and_compare(td.text.strip()) if td.text else None for td in tds]
                        #data.append(row)
                        col1 = self.extract_and_compare(tds[0].text.strip(), "1.2.250.1.213.1.6.1.126") if tds[0].text else None
                        col2 = self.extract_and_compare(tds[1].text.strip(), "1.2.250.1.213.1.6.1.127") if tds[1].text else None
                        col3 = self.extract_and_compare(tds[2].text.strip(), "1.2.250.1.213.1.6.1.128") if tds[2].text else None
                        data.append([col1, col2, col3])

            # Création du DataFrame
            df = pd.DataFrame(data, columns=["activité", "modalité", "forme"])

            #print(df)
            return df
        else:
            print("Analyseur: réponse vide ou erreur pour l'onglet AMF.")

    def manipulReponseDiscipline(self, response_text):
        """Traitement de la réponse pour l'onglet Discipline."""
        if response_text:
            print("Analyseur: traitement de l'onglet Discipline...")

            """
            Analyse et extrait les données des Discipline (Discipline, Mode de fonctionnement, Clientèle, Homme, Femme, Total) de la réponse XML pour l'onglet Discipline.
            :param response_text: Texte de la réponse XML.
            :return: Un DataFrame pandas avec les colonnes ["discipline", "mode de fonctionnement", "clientèle", "homme", "femme", "total"].
            """
            # Remplacements dans la chaîne
            """
            replacements = {
                "&eacute;": "é",
                "&egrave;": "è",
                "&nbsp;": "",
                "&gt;": "",
                "&deg;": "",
                "&ecirc;": "",
                "&oldFiness=": "&amp;oldFiness=",
                "&agrave;": "à",
                "&": "&amp;",  # Doit être fait en dernier pour éviter les conflits
                "colspan=2": 'colspan="2"',
            }
            """

            for old, new in self.replacements.items():
                response_text = response_text.replace(old, new)

            # Conversion en XML
            try:
                xml_bytes = response_text.encode('utf-8')
                root = etree.fromstring(xml_bytes)
            except etree.ParseError as e:
                print(f"Erreur de parsing XML : {e}")
                return pd.DataFrame(columns=["discipline", "mode de fonctionnement", "clientèle", "homme", "femme", "total"])

            # Recherche de la table et extraction des données
            data = []
            table = root.xpath("//table")[0]
            if table is not None:
                rows = table.xpath(".//tr[position() > 1]")
                for tr in rows:
                    tds = tr.findall(".//td")
                    if len(tds) == 6:  # Chaque ligne doit avoir exactement 3 colonnes
                        row = [td.text.strip() if td.text else "" for td in tds]
                        data.append(row)

            # Création du DataFrame
            df = pd.DataFrame(data, columns=["discipline", "mode de fonctionnement", "clientèle", "homme", "femme", "total"])
            #print(df)
            return df
        else:
            print("Analyseur: réponse vide ou erreur pour l'onglet Discipline.")

    def manipulReponseEML(self, response_text):
        """Traitement de la réponse pour l'onglet EML."""
        if response_text:
            print("Analyseur: traitement de l'onglet EML...")
            """
            Analyse et extrait les données des Equipements Médicaux Lourds (EML, libellé, libellé long) de la réponse XML pour l'onglet EML.
            :param response_text: Texte de la réponse XML.
            :return: Un DataFrame pandas avec les colonnes ["eml", "libellé", "libellé_long"].
            """
            # Remplacements dans la chaîne
            """
            replacements = {
                "&eacute;": "é",
                "&egrave;": "è",
                "&nbsp;": "",
                "&gt;": "",
                "&deg;": "",
                "&ecirc;": "",
                "&oldFiness=": "&amp;oldFiness=",
                "&agrave;": "à",
                "&": "&amp;",  # Doit être fait en dernier pour éviter les conflits
                "colspan=2": 'colspan="2"',
            }
            """

            for old, new in self.replacements.items():
                response_text = response_text.replace(old, new)

            # Conversion en XML
            try:
                xml_bytes = response_text.encode('utf-8')
                root = etree.fromstring(xml_bytes)
            except etree.ParseError as e:
                print(f"Erreur de parsing XML : {e}")
                return pd.DataFrame(columns=["eml", "libellé", "libellé_long"])

            # Recherche de la table et extraction des données
            data = []
            table = root.xpath("//table")[0]
            if table is not None:
                rows = table.xpath(".//tr[position() > 1]")
                for tr in rows:
                    tds = tr.findall(".//td")
                    if len(tds) == 3:  # Chaque ligne doit avoir exactement 3 colonnes
                        row = [td.text.strip() if td.text else "" for td in tds]
                        data.append(row)

            # Création du DataFrame
            df = pd.DataFrame(data, columns=["eml", "libellé", "libellé_long"])
            #print(df)
            return df
        else:
            print("Analyseur: réponse vide ou erreur pour l'onglet EML.")

    def manipulReponseFormation(self, response_text):
        """Traitement de la réponse pour l'onglet Formation."""
        if response_text:
            print("Analyseur: traitement de l'onglet Formation...")
            """
            Analyse et extrait les données des Formations (libellé, capacité) de la réponse XML pour l'onglet Formations.
            :param response_text: Texte de la réponse XML.
            :return: Un DataFrame pandas avec les colonnes ["libellé", "capacité"].
            """
            # Remplacements dans la chaîne
            """
            replacements = {
                "&eacute;": "é",
                "&egrave;": "è",
                "&nbsp;": "",
                "&gt;": "",
                "&deg;": "",
                "&ecirc;": "",
                "&oldFiness=": "&amp;oldFiness=",
                "&agrave;": "à",
                "&": "&amp;",  # Doit être fait en dernier pour éviter les conflits
                "colspan=2": 'colspan="2"',
                "width = ": "width=",
                "styleTitre\"width": "styleTitre\" width"
            }
            """

            for old, new in self.replacements.items():
                response_text = response_text.replace(old, new)

            # Conversion en XML
            try:
                xml_bytes = response_text.encode('utf-8')
                root = etree.fromstring(xml_bytes)
            except etree.ParseError as e:
                print(f"Erreur de parsing XML : {e}")
                return pd.DataFrame(columns=["libellé", "capacité"])

            # Recherche de la table et extraction des données
            data = []
            table = root.xpath("//table")[0]
            if table is not None:
                rows = table.xpath(".//tr[position() > 1]")
                for tr in rows:
                    tds = tr.findall(".//td")
                    if len(tds) == 2:  # Chaque ligne doit avoir exactement 3 colonnes
                        row = [td.text.strip() if td.text else "" for td in tds]
                        data.append(row)

            # Création du DataFrame
            df = pd.DataFrame(data, columns=["libellé", "capacité"])
            #print(df)
            return df
        else:
            print("Analyseur: réponse vide ou erreur pour l'onglet Formation.")

    def manipulReponseAMM(self, response_text):
        """Traitement de la réponse pour l'onglet AMM."""
        if response_text:
            print("Analyseur: traitement de l'onglet AMM...")
            """
            Analyse et extrait les données des AMM (Activité, Modalité, Mention, Pratiques thérapeutiques spécifiques, Déclaration) de la réponse XML pour l'onglet AMM.
            :param response_text: Texte de la réponse XML.
            :return: Un DataFrame pandas avec les colonnes ["activité", "modalité", "mention", "pratiques thérapeutiques spécifiques", "déclaration"].
            """
            # Remplacements dans la chaîne
            """
            replacements = {
                "&eacute;": "é",
                "&egrave;": "è",
                "&nbsp;": "",
                "&gt;": "",
                "&deg;": "",
                "&ecirc;": "",
                "&oldFiness=": "&amp;oldFiness=",
                "&agrave;": "à",
                "&": "&amp;",  # Doit être fait en dernier pour éviter les conflits
                "colspan=2": 'colspan="2"',
            }
            """

            for old, new in self.replacements.items():
                response_text = response_text.replace(old, new)

            # Conversion en XML
            try:
                xml_bytes = response_text.encode('utf-8')
                root = etree.fromstring(xml_bytes)
            except etree.ParseError as e:
                print(f"Erreur de parsing XML : {e}")
                return pd.DataFrame(columns=["activité", "modalité", "mention", "pratiques thérapeutiques spécifiques", "déclaration"])

            # Recherche de la table et extraction des données
            data = []
            table = root.xpath("//table")[0]
            if table is not None:
                rows = table.xpath(".//tr[position() > 1]")
                for tr in rows:
                    tds = tr.findall(".//td")
                    if len(tds) == 5:  # Chaque ligne doit avoir exactement 3 colonnes
                        row = [td.text.strip() if td.text else "" for td in tds]
                        data.append(row)

            # Création du DataFrame
            df = pd.DataFrame(data, columns=["activité", "modalité", "mention", "pratiques thérapeutiques spécifiques", "déclaration"])
            #print(df)
            return df
        else:
            print("Analyseur: réponse vide ou erreur pour l'onglet AMM.")


class ConnexionFiness:
    def __init__(self, analyseur):
        self.session = requests.Session()
        self.jsessionid = None
        self.host = "finess.esante.gouv.fr"
        self.base_url =       "https://" + self.host + "/fininter/jsp/rechercheSimple.jsp?coche=ok"
        self.post_url =       "https://" + self.host + "/fininter/jsp/actionRechercheSimple.do"
        self.get_urls = {
            FinessET.CONTEXT_IDENTIFICATION: "https://" + self.host + "/fininter/jsp/DetailOnglet.view?&q=1&_=",
            FinessET.CONTEXT_AMF:            "https://" + self.host + "/fininter/jsp/DetailOnglet.view?&q=2&_=",
            FinessET.CONTEXT_DISCIPLINE:     "https://" + self.host + "/fininter/jsp/DetailOnglet.view?&q=3&_=",
            FinessET.CONTEXT_EML:            "https://" + self.host + "/fininter/jsp/DetailOnglet.view?&q=4&_=",
            FinessET.CONTEXT_FORMATION:      "https://" + self.host + "/fininter/jsp/DetailOnglet.view?&q=5&_=",
            FinessET.CONTEXT_AMM:            "https://" + self.host + "/fininter/jsp/DetailOnglet.view?&q=6&_=",
        }
        self.analyseur = analyseur
        self.max_retries = 3
        self.retry_delay = 1
        self.too_many_requests_delay = 1
        self.useragent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36"

    def __enter__(self):
        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, sdch",
            "Connection": "keep-alive",
            "User-Agent": self.useragent,
            "DNT": "1",
            "Host": self.host,
            "Referer": "",
        }

        print("Connexion à FINESS...")
        response = self.session.get(self.base_url, headers=headers)

        if response.status_code == 200:
            cookies = self.session.cookies.get_dict()
            self.jsessionid = cookies.get("JSESSIONID")
            if self.jsessionid:
                print(f"JSESSIONID obtenu : {self.jsessionid}")
            else:
                raise ValueError("JSESSIONID introuvable dans les cookies.")
        else:
            raise ConnectionError(f"Erreur lors de la connexion : {response.status_code}")
        
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        print("Fermeture de la connexion à FINESS.")
        self.session.close()
        return False

    def _handle_response(self, response, context):
        """Gère les réponses HTTP avec retries pour les erreurs 5xx et pauses pour 429."""
        retries = 0
        while retries <= self.max_retries:
            if response.status_code == 200:
                return self.analyseur.manipulReponse(response.text, context)
            elif 500 <= response.status_code < 600:
                print(f"Erreur serveur {response.status_code}, retry dans {self.retry_delay} seconde(s)...")
                retries += 1
                time.sleep(self.retry_delay)
            elif response.status_code == 429:
                print(f"Erreur 429 (Trop de requêtes), pause de {self.too_many_requests_delay} seconde(s)...")
                time.sleep(self.too_many_requests_delay)
            elif 400 <= response.status_code < 500:
                print(f"Erreur client {response.status_code}, réponse vide transmise aux hooks.")
                self.analyseur.manipulReponse("", context)
                return
            else:
                raise ConnectionError(f"Erreur inattendue : {response.status_code}")

    def fetch_data(self, noFinessEt):
        if not self.jsessionid:
            raise RuntimeError("Pas de JSESSIONID. Assurez-vous d'utiliser 'with' pour établir la connexion.")

        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, sdch",
            "Connection": "keep-alive",
            "User-Agent": self.useragent,
            "DNT": "1",
            "Host": self.host,
            "Referer": "",
            "Content-Type": "application/x-www-form-urlencoded",
        }

        payload = {
            "siren": "",
            "siret": "",
            "nofiness": noFinessEt,
            "rs": "",
            "ongletActuel": "1",
            "regionText": "",
            "region": "",
            "deptriorder": "",
            "dep": "",
            "commune": "",
            "rue": "",
            "choixLoc": "",
        }

        """"
        print(f"Envoi d'une requête POST avec noFinessEt = {noFinessEt}...")
        response = self.session.post(self.post_url, headers=headers, data=payload)
        self._handle_response(response, context="rechercheSimple")

        # Fetch des onglets via GET
        for context, url in self.get_urls.items():
            print(f"Fetching {context}...")
            response = self.session.get(url, headers=headers)
            self._handle_response(response, context)
        """
        print(f"Envoi d'une requête POST avec noFinessEt = {noFinessEt}...")
        response = self.session.post(self.post_url, headers=headers, data=payload)
        # Appel de la méthode manipulReponse pour traiter la réponse principale
  
        dataframes = {FinessET.CONTEXT_RECHERCHE_SIMPLE: self._handle_response(response, context=FinessET.CONTEXT_RECHERCHE_SIMPLE)}

        # Fetch des onglets via GET et traitement des réponses
        for context, url in self.get_urls.items():
            print(f"Fetching {context}...")
            response = self.session.get(url, headers=headers)
            dataframes[context] = self._handle_response(response, context)

        # Conversion en DataFrame contenant des DataFrames comme valeurs
        result_df = pd.DataFrame([{**{"noFinessET": noFinessEt}, **dataframes}])

        return result_df


if __name__ == "__main__":
    pass

    """
    analyseur = AnalyseurFinessET()
    # noFinessEt = "010001741"  # Exemple de paramètre noFinessEt

    with ConnexionFiness(analyseur) as connexion:
        print("Connexion établie.")
        #necker: amf, eml
        noFinessEt = "750100208"
        ret = connexion.fetch_data(noFinessEt)
        print(f"DataFrames collectés pour noFinessET {noFinessEt}:")
        for key, df in ret.items():
            print(f"Contexte: {key}, DataFrame:")
            print(df)
        # amm
        noFinessEt = "010000024"
        print(connexion.fetch_data(noFinessEt))
        #discipline
        noFinessEt = "010002319"
        print(connexion.fetch_data(noFinessEt))
        #formation
        noFinessEt = "020004594"
        ret = connexion.fetch_data(noFinessEt)
        print(f"DataFrames collectés pour noFinessET {noFinessEt}:")
        for key, df in ret.items():
            print(f"Contexte: {key}, DataFrame:")
            print(df)
    """
# irene

SDO = standards development organization
RA = registration authority

## ICHI (actes)

ICHI, SDO = WHO: browse at http://bioportal.lirmm.fr/ontologies/ICHI

## ISO-3166 (pays)

ISO 3166, SDO = ISO: browse at https://www.iso.org/obp/ui/fr/#search

Homologation de la ISO 3166-1 : https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000000641257

## ISO-639 (langues)
ISO-639-2, RA: Library Of Congress: https://www.loc.gov/standards/iso639-2/langhome.html

ISO-639-3, RA: SIL.org: https://iso639-3.sil.org/code_tables/639/data/f

ISO-639-5, RA: Library Of Congress: https://www.loc.gov/standards/iso639-5/#:~:text=ISO%20639%2D5%20Registration%20Authority%20%2D%20Library%20of%20Congress&text=The%20Library%20of%20Congress%20has,for%20language%20families%20and%20groups.


Homologation de la ISO 639-1 alpha2: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000000594717

Homologation de la ISO 639-3: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000000822499

Homologation de la ISO 639-5: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000019114008

## ISO-15924 (écriture)

(non-homologuée)
